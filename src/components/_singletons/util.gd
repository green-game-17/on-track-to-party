extends Node

func format_count(count: int) -> String:
	var text = str(count)

	if count >= 1_000_000_000:
		text = 'a lot'
	elif count >= 1_000_000:
		text = '%s.%03d.%03d' % [
			floor(count / 1_000_000), floor((count % 1_000_000) / 1_000), count % 1_000
		]
	elif count >= 1_000:
		text = '%s.%03d' % [floor(count / 1_000), count % 1_000]

	return text
