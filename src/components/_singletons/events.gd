extends Node2D


var debugSignals = true


# Signals:

signal depart_station()
signal select_junction(selected_path)
signal passed_junction()
signal arrive_station()
signal changed_current_station(id)
signal inventory_changed()
signal money_changed()
signal passengers_changed()

signal set_train()
signal set_carriages()


func _ready():
	if debugSignals:
		for s in self.get_signal_list():
			if s.args.empty():
				connect(s.name, self, "__on_signal_no_args", [s.name])
			elif s.args.size() == 1:
				connect(s.name, self, "__on_signal_one_args", [s.name])
			elif s.args.size() == 2:
				connect(s.name, self, "__on_signal_two_args", [s.name])
			else:
				push_error('[EventManager]! %s name has more than 2 args, debug not set up!' % s.name)


func __on_signal_no_args(name):
	print('[EventManager]: got signal %s' % name)


func __on_signal_one_args(arg, name):
	print('[EventManager]: got signal %s with arg %s' % [name, arg])


func __on_signal_two_args(arg1, arg2, name):
	print('[EventManager]: got signal %s with args %s , %s' % [name, arg1, arg2])
