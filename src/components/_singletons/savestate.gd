extends Node


const FILE_SAVE_PATH = 'user://on_track_to_party_savegame.txt'

var __savestate = {
	'volume': 80,
	'blur_active': true,
	'saves': []
}


func _ready():
	__load_data()


func set_volume(new_volume: int, save: bool = true):
	__savestate.volume = new_volume
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index('Master'), new_volume - 80)
	if save:
		__save_data()


func get_volume() -> int:
	return __savestate.volume


func set_blur(is_blur_active: bool, save: bool = true):
	__savestate.blur_active = is_blur_active
	STATE.set_blur(is_blur_active)
	if save:
		__save_data()


func get_blur() -> bool:
	return __savestate.blur_active


func generate_save(save_name: String):
	if save_name == '':
		save_name = 'Savestate' + str(__savestate.saves.size() + 1)
	var current_time = OS.get_datetime()
	current_time.day = '%02d' % current_time.day
	current_time.month = '%02d' % current_time.month
	current_time.hour = '%02d' % current_time.hour
	current_time.minute = '%02d' % current_time.minute
	__savestate.saves.append({
		'id': __savestate.saves.size() + 1,
		'name': save_name,
		'date': '{day}.{month}.{year} - {hour}:{minute}'.format(current_time),
	})
	__save_data()


func get_saves():
	return __savestate.saves


func __save_data():
	var file = File.new()
	file.open(FILE_SAVE_PATH, File.WRITE)
	file.store_line(to_json(__savestate))
	file.close()


func __load_data():
	var file = File.new()
	if file.file_exists(FILE_SAVE_PATH):
		file.open(FILE_SAVE_PATH, File.READ)
		var loaded_data = parse_json(file.get_line())
		file.close()
		
		for key in __savestate.keys():
			var value = loaded_data.get(key, __savestate.get(key))
			match key:
				'volume':
					set_volume(value, false)
				'blur_active':
					set_blur(value, false)
				'saves':
					__savestate.saves = value
