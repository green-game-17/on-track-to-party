extends Node


const MIN_DIST := 40
const MAX_DIST := 220

var MIN_DIST_SQUARED := pow(MIN_DIST, 2)
var MAX_DIST_SQUARED := pow(MAX_DIST, 2)
var rng : RandomNumberGenerator = RandomNumberGenerator.new()
var network : Dictionary = {
	0: {
		'name': null,
		'pos': Vector2.ZERO,
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [15, 91, 32],
			'right': [86, 63],
			'express': [88, 58]
		},
		'is_main_station': true,
		'biome': ID.BIOME_PLAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': true,
	},
	1: {
		'name': null,
		'pos': Vector2(255, 145),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [9, 17, 76],
			'right': [98, 7, 69]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	2: {
		'name': null,
		'pos': Vector2(-500, 34),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [38, 47],
			'right': [40, 13, 90]
		},
		'is_main_station': false,
		'biome': ID.BIOME_MOUNTAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	3: {
		'name': null,
		'pos': Vector2(-111, 58),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [87, 91],
			'right': [95, 57, 12]
		},
		'is_main_station': false,
		'biome': ID.BIOME_PLAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	4: {
		'name': null,
		'pos': Vector2(121, 356),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [78, 80],
			'right': [31, 19]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	5: {
		'name': null,
		'pos': Vector2(219, -350),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [74, 46, 20],
			'right': [77, 35]
		},
		'is_main_station': false,
		'biome': ID.BIOME_VOLCANO,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	6: {
		'name': null,
		'pos': Vector2(-39, -475),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [77, 29],
			'right': [33, 81]
		},
		'is_main_station': false,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	7: {
		'name': null,
		'pos': Vector2(288, 371),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [98, 1, 69],
			'right': [85, 26, 27]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	8: {
		'name': null,
		'pos': Vector2(-382, 354),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [90, 37, 89],
			'right': [40, 14]
		},
		'is_main_station': false,
		'biome': ID.BIOME_UNDERWATER,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	9: {
		'name': null,
		'pos': Vector2(153, 150),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [17, 1, 76],
			'right': [75, 18]
		},
		'is_main_station': false,
		'biome': ID.BIOME_PLAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	10: {
		'name': null,
		'pos': Vector2(435, -173),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [56, 34],
			'right': [83, 45]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINBOW_ROAD,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	11: {
		'name': null,
		'pos': Vector2(299, 37),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [63, 17],
			'right': [84, 59, 53]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	12: {
		'name': null,
		'pos': Vector2(-240, 32),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [95, 57, 3],
			'right': [48, 61]
		},
		'is_main_station': false,
		'biome': ID.BIOME_MOUNTAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	13: {
		'name': null,
		'pos': Vector2(-447, 152),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [40, 90, 2],
			'right': [21, 97, 61]
		},
		'is_main_station': false,
		'biome': ID.BIOME_MOUNTAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	14: {
		'name': null,
		'pos': Vector2(-421, 254),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [49, 21, 25],
			'right': [40, 8]
		},
		'is_main_station': false,
		'biome': ID.BIOME_UNDERWATER,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	15: {
		'name': null,
		'pos': Vector2(-58, -32),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [60, 87, 96],
			'right': [0, 91, 32]
		},
		'is_main_station': false,
		'biome': ID.BIOME_PLAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	16: {
		'name': null,
		'pos': Vector2(-356, -344),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [94, 62, 81],
			'right': [71, 38]
		},
		'is_main_station': false,
		'biome': ID.BIOME_DESERT,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	17: {
		'name': null,
		'pos': Vector2(240, 90),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [11, 63],
			'right': [9, 1, 76]
		},
		'is_main_station': false,
		'biome': ID.BIOME_PLAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	18: {
		'name': null,
		'pos': Vector2(107, 189),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [75, 9],
			'right': [82, 78, 31]
		},
		'is_main_station': false,
		'biome': ID.BIOME_CITY,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	19: {
		'name': null,
		'pos': Vector2(15, 343),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [31, 4],
			'right': [93, 54, 44]
		},
		'is_main_station': false,
		'biome': ID.BIOME_CITY,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	20: {
		'name': null,
		'pos': Vector2(318, -500),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [65, 73],
			'right': [5, 46, 74]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINBOW_ROAD,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	21: {
		'name': null,
		'pos': Vector2(-418, 191),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [14, 49, 25],
			'right': [13, 97, 61]
		},
		'is_main_station': false,
		'biome': ID.BIOME_MOUNTAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	22: {
		'name': null,
		'pos': Vector2(-352, -74),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [23, 72, 50],
			'right': [97, 47, 48]
		},
		'is_main_station': false,
		'biome': ID.BIOME_DESERT,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	23: {
		'name': null,
		'pos': Vector2(-182, -111),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [60, 57, 64],
			'right': [50, 72, 22]
		},
		'is_main_station': false,
		'biome': ID.BIOME_DESERT,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	24: {
		'name': null,
		'pos': Vector2(158, -249),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [84, 74, 99],
			'right': [51, 41, 35],
			'express': [69, 97]
		},
		'is_main_station': true,
		'biome': ID.BIOME_VOLCANO,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	25: {
		'name': null,
		'pos': Vector2(-280, 172),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [95, 55, 30],
			'right': [14, 21, 49]
		},
		'is_main_station': false,
		'biome': ID.BIOME_MOUNTAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	26: {
		'name': null,
		'pos': Vector2(487, 254),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [92, 70],
			'right': [85, 7, 27]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	27: {
		'name': null,
		'pos': Vector2(344, 288),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [98, 92],
			'right': [85, 7, 26]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	28: {
		'name': null,
		'pos': Vector2(-2, 181),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [55, 32, 75],
			'right': [82, 58]
		},
		'is_main_station': false,
		'biome': ID.BIOME_CITY,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	29: {
		'name': null,
		'pos': Vector2(-172, -403),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [6, 77],
			'right': [33, 94]
		},
		'is_main_station': false,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	30: {
		'name': null,
		'pos': Vector2(-204, 248),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [58, 66, 44],
			'right': [95, 25, 55]
		},
		'is_main_station': false,
		'biome': ID.BIOME_UNDERWATER,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	31: {
		'name': null,
		'pos': Vector2(114, 299),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [82, 78, 18],
			'right': [4, 19]
		},
		'is_main_station': false,
		'biome': ID.BIOME_CITY,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	32: {
		'name': null,
		'pos': Vector2(-71, 112),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [55, 28, 75],
			'right': [15, 91, 0]
		},
		'is_main_station': false,
		'biome': ID.BIOME_PLAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	33: {
		'name': null,
		'pos': Vector2(-112, -455),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [81, 6],
			'right': [29, 94]
		},
		'is_main_station': false,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	34: {
		'name': null,
		'pos': Vector2(416, -53),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [59, 76, 70],
			'right': [56, 10]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINBOW_ROAD,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	35: {
		'name': null,
		'pos': Vector2(23, -297),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [51, 41, 24],
			'right': [77, 5]
		},
		'is_main_station': false,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	36: {
		'name': null,
		'pos': Vector2(-177, 415),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [54, 89],
			'right': [66, 37, 49]
		},
		'is_main_station': false,
		'biome': ID.BIOME_UNDERWATER,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	37: {
		'name': null,
		'pos': Vector2(-247, 354),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [36, 66, 49],
			'right': [90, 8, 89]
		},
		'is_main_station': false,
		'biome': ID.BIOME_UNDERWATER,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	38: {
		'name': null,
		'pos': Vector2(-472, -309),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [16, 71],
			'right': [2, 47]
		},
		'is_main_station': false,
		'biome': ID.BIOME_DESERT,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	39: {
		'name': null,
		'pos': Vector2(425, -365),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [42, 79, 99],
			'right': [73, 46]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINBOW_ROAD,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	40: {
		'name': null,
		'pos': Vector2(-461, 295),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [90, 13, 2],
			'right': [8, 14]
		},
		'is_main_station': false,
		'biome': ID.BIOME_UNDERWATER,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	41: {
		'name': null,
		'pos': Vector2(38, -237),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [51, 24, 35],
			'right': [88, 50, 64]
		},
		'is_main_station': false,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	42: {
		'name': null,
		'pos': Vector2(444, -313),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [45, 56, 65],
			'right': [39, 79, 99]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINBOW_ROAD,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	43: {
		'name': null,
		'pos': Vector2(169, 428),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [93, 85],
			'right': [69, 80]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	44: {
		'name': null,
		'pos': Vector2(-91, 395),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [93, 54, 19],
			'right': [58, 66, 30]
		},
		'is_main_station': false,
		'biome': ID.BIOME_CITY,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	45: {
		'name': null,
		'pos': Vector2(443, -249),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [83, 10],
			'right': [56, 42, 65]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINBOW_ROAD,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	46: {
		'name': null,
		'pos': Vector2(338, -432),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [73, 39],
			'right': [5, 74, 20]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINBOW_ROAD,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	47: {
		'name': null,
		'pos': Vector2(-421, -101),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [38, 2],
			'right': [97, 48, 22]
		},
		'is_main_station': false,
		'biome': ID.BIOME_DESERT,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	48: {
		'name': null,
		'pos': Vector2(-288, -28),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [12, 61],
			'right': [97, 47, 22]
		},
		'is_main_station': false,
		'biome': ID.BIOME_MOUNTAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	49: {
		'name': null,
		'pos': Vector2(-318, 300),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [36, 37, 66],
			'right': [14, 21, 25],
			'express': [88, 69]
		},
		'is_main_station': true,
		'biome': ID.BIOME_UNDERWATER,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	50: {
		'name': null,
		'pos': Vector2(-160, -233),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [88, 41, 64],
			'right': [23, 72, 22]
		},
		'is_main_station': false,
		'biome': ID.BIOME_DESERT,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	51: {
		'name': null,
		'pos': Vector2(-4, -156),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [24, 41, 35],
			'right': [86, 96]
		},
		'is_main_station': false,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	52: {
		'name': null,
		'pos': Vector2(-245, -273),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [67, 88],
			'right': [68, 71, 72]
		},
		'is_main_station': false,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	53: {
		'name': null,
		'pos': Vector2(389, -102),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [84, 59, 11],
			'right': [79, 83]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINBOW_ROAD,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	54: {
		'name': null,
		'pos': Vector2(-45, 473),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [93, 19, 44],
			'right': [36, 89]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	55: {
		'name': null,
		'pos': Vector2(-98, 162),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [32, 28, 75],
			'right': [95, 25, 30]
		},
		'is_main_station': false,
		'biome': ID.BIOME_UNDERWATER,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	56: {
		'name': null,
		'pos': Vector2(464, -125),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [34, 10],
			'right': [45, 42, 65]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINBOW_ROAD,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	57: {
		'name': null,
		'pos': Vector2(-189, -53),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [95, 3, 12],
			'right': [60, 64, 23]
		},
		'is_main_station': false,
		'biome': ID.BIOME_PLAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	58: {
		'name': null,
		'pos': Vector2(9, 231),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [28, 82],
			'right': [44, 66, 30],
			'express': [0, 72]
		},
		'is_main_station': true,
		'biome': ID.BIOME_CITY,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	59: {
		'name': null,
		'pos': Vector2(345, 15),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [84, 11, 53],
			'right': [76, 34, 70]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	60: {
		'name': null,
		'pos': Vector2(-89, -99),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [96, 87, 15],
			'right': [64, 57, 23]
		},
		'is_main_station': false,
		'biome': ID.BIOME_PLAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	61: {
		'name': null,
		'pos': Vector2(-268, 106),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [13, 97, 21],
			'right': [48, 12]
		},
		'is_main_station': false,
		'biome': ID.BIOME_MOUNTAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	62: {
		'name': null,
		'pos': Vector2(-309, -369),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [94, 81, 16],
			'right': [67, 68]
		},
		'is_main_station': false,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	63: {
		'name': null,
		'pos': Vector2(165, 21),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [0, 86],
			'right': [11, 17]
		},
		'is_main_station': false,
		'biome': ID.BIOME_PLAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	64: {
		'name': null,
		'pos': Vector2(-149, -162),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [88, 50, 41],
			'right': [60, 57, 23]
		},
		'is_main_station': false,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	65: {
		'name': null,
		'pos': Vector2(500, -475),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [45, 42, 56],
			'right': [20, 73]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINBOW_ROAD,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	66: {
		'name': null,
		'pos': Vector2(-172, 303),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [58, 44, 30],
			'right': [36, 37, 49]
		},
		'is_main_station': false,
		'biome': ID.BIOME_UNDERWATER,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	67: {
		'name': null,
		'pos': Vector2(-161, -338),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [88, 52],
			'right': [62, 68]
		},
		'is_main_station': false,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	68: {
		'name': null,
		'pos': Vector2(-245, -320),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [62, 67],
			'right': [71, 52, 72]
		},
		'is_main_station': false,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	69: {
		'name': null,
		'pos': Vector2(241, 327),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [98, 7, 1],
			'right': [43, 80],
			'express': [49, 24]
		},
		'is_main_station': true,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	70: {
		'name': null,
		'pos': Vector2(459, 143),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [59, 34, 76],
			'right': [92, 26]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	71: {
		'name': null,
		'pos': Vector2(-379, -297),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [16, 38],
			'right': [68, 52, 72]
		},
		'is_main_station': false,
		'biome': ID.BIOME_DESERT,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	72: {
		'name': null,
		'pos': Vector2(-248, -204),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [68, 52, 71],
			'right': [23, 50, 22],
			'express': [58, 99]
		},
		'is_main_station': true,
		'biome': ID.BIOME_DESERT,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	73: {
		'name': null,
		'pos': Vector2(357, -471),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [20, 65],
			'right': [39, 46]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINBOW_ROAD,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	74: {
		'name': null,
		'pos': Vector2(235, -304),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [24, 84, 99],
			'right': [5, 46, 20]
		},
		'is_main_station': false,
		'biome': ID.BIOME_VOLCANO,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	75: {
		'name': null,
		'pos': Vector2(-12, 130),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [55, 28, 32],
			'right': [9, 18]
		},
		'is_main_station': false,
		'biome': ID.BIOME_PLAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	76: {
		'name': null,
		'pos': Vector2(362, 128),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [9, 1, 17],
			'right': [59, 34, 70]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	77: {
		'name': null,
		'pos': Vector2(-52, -333),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [5, 35],
			'right': [6, 29]
		},
		'is_main_station': false,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	78: {
		'name': null,
		'pos': Vector2(141, 235),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [82, 18, 31],
			'right': [80, 4]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	79: {
		'name': null,
		'pos': Vector2(346, -231),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [53, 83],
			'right': [39, 42, 99]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINBOW_ROAD,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	80: {
		'name': null,
		'pos': Vector2(208, 274),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [69, 43],
			'right': [78, 4]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	81: {
		'name': null,
		'pos': Vector2(-274, -496),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [33, 6],
			'right': [94, 62, 16]
		},
		'is_main_station': false,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	82: {
		'name': null,
		'pos': Vector2(68, 272),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [18, 78, 31],
			'right': [28, 58]
		},
		'is_main_station': false,
		'biome': ID.BIOME_CITY,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	83: {
		'name': null,
		'pos': Vector2(407, -206),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [79, 53],
			'right': [10, 45]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINBOW_ROAD,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	84: {
		'name': null,
		'pos': Vector2(330, -119),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [11, 59, 53],
			'right': [24, 74, 99]
		},
		'is_main_station': false,
		'biome': ID.BIOME_VOLCANO,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	85: {
		'name': null,
		'pos': Vector2(399, 401),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [26, 7, 27],
			'right': [93, 43]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	86: {
		'name': null,
		'pos': Vector2(45, -14),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [0, 63],
			'right': [51, 96]
		},
		'is_main_station': false,
		'biome': ID.BIOME_PLAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	87: {
		'name': null,
		'pos': Vector2(-138, 12),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [60, 96, 15],
			'right': [91, 3]
		},
		'is_main_station': false,
		'biome': ID.BIOME_PLAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	88: {
		'name': null,
		'pos': Vector2(-63, -271),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [41, 50, 64],
			'right': [67, 52],
			'express': [49, 0]
		},
		'is_main_station': true,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	89: {
		'name': null,
		'pos': Vector2(-202, 498),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [36, 54],
			'right': [90, 37, 8]
		},
		'is_main_station': false,
		'biome': ID.BIOME_UNDERWATER,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	90: {
		'name': null,
		'pos': Vector2(-478, 397),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [8, 37, 89],
			'right': [40, 13, 2]
		},
		'is_main_station': false,
		'biome': ID.BIOME_UNDERWATER,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	91: {
		'name': null,
		'pos': Vector2(-70, 39),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [87, 3],
			'right': [15, 0, 32]
		},
		'is_main_station': false,
		'biome': ID.BIOME_PLAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	92: {
		'name': null,
		'pos': Vector2(436, 218),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [27, 98],
			'right': [70, 26]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	93: {
		'name': null,
		'pos': Vector2(57, 485),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [85, 43],
			'right': [19, 54, 44]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	94: {
		'name': null,
		'pos': Vector2(-281, -412),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [29, 33],
			'right': [81, 62, 16]
		},
		'is_main_station': false,
		'biome': ID.BIOME_SEASIDE,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	95: {
		'name': null,
		'pos': Vector2(-165, 86),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [55, 25, 30],
			'right': [3, 57, 12]
		},
		'is_main_station': false,
		'biome': ID.BIOME_MOUNTAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	96: {
		'name': null,
		'pos': Vector2(-39, -72),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [60, 87, 15],
			'right': [51, 86]
		},
		'is_main_station': false,
		'biome': ID.BIOME_PLAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	97: {
		'name': null,
		'pos': Vector2(-362, 62),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [13, 21, 61],
			'right': [48, 47, 22],
			'express': [24, 99]
		},
		'is_main_station': true,
		'biome': ID.BIOME_MOUNTAINS,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	98: {
		'name': null,
		'pos': Vector2(306, 237),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [1, 7, 69],
			'right': [27, 92]
		},
		'is_main_station': false,
		'biome': ID.BIOME_RAINFOREST,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
	99: {
		'name': null,
		'pos': Vector2(322, -288),
		'center_points': {
			'left': Vector2.ZERO,
			'right': Vector2.ZERO
		},
		'connections': {
			'left': [24, 74, 84],
			'right': [39, 79, 42],
			'express': [72, 97]
		},
		'is_main_station': true,
		'biome': ID.BIOME_RAINBOW_ROAD,
		'is_passenger_station': false,
		'has_materials': [],
		'visible': false,
	},
}

const STATION_NAMES = {
	'first': [
		'Chase',
		'Cheese',
		'West',
		'North',
		'East',
		'South',
		'Arrow',
		'Stone',
		'Tower',
		'Black',
		'Green',
		'Red',
		'Blue',
		'White',
		'Bamboo',
		'Copper',
		'Silver',
		'Gold',
		'New',
		'Grand',
		'Small',
		'Boulder',
		'Iron',
		'Cotton'
	],
	'last': [
		'water',
		' Shore',
		' Coast',
		' Meadows',
		' Loch',
		' Lake',
		' Bridge',
		'vale',
		' Point',
		' Haven',
		'house',
		' Gardens',
		'field',
		' Hills',
		' Station',
		'bark',
		' Butt'
	]
}


func _ready():
	rng.randomize()
	calculate_center_points()
	generate_station_infos()
#	get_numbers()
#	print_network()


func calculate_center_points():
	for i in network:
		var station = network.get(i)
		
		for j in station.connections.left:
			var goal_data = network.get(j)
			station.center_points.left += goal_data.pos
		station.center_points.left += station.pos
		station.center_points.left /= station.connections.left.size() + 1
		
		for j in station.connections.right:
			var goal_data = network.get(j)
			station.center_points.right += goal_data.pos
		station.center_points.right += station.pos
		station.center_points.right /= station.connections.right.size() + 1


func generate_station_infos():
	var generated_names = []
	var sorted_stations = {}
	for i in network.size():
		var station = network.get(i)
		if station.name == null:
			var rand_first = rng.randi() % STATION_NAMES.first.size()
			var rand_last = rng.randi() % STATION_NAMES.last.size()
			var new_name = STATION_NAMES.first[rand_first] + STATION_NAMES.last[rand_last]
			while new_name in generated_names:
				rand_first = rng.randi() % STATION_NAMES.first.size()
				rand_last = rng.randi() % STATION_NAMES.last.size()
				new_name = STATION_NAMES.first[rand_first] + STATION_NAMES.last[rand_last]
			generated_names.append(new_name)
			station.name = new_name
		if not sorted_stations.has(station.biome):
			sorted_stations[station.biome] = []
		sorted_stations[station.biome].append(i)
	
	var common_items = DATA.ITEMS.COMMON_ITEM
	for biome in sorted_stations:
		var special_items = DATA.ITEMS[biome]
		var item_pool = special_items.duplicate()
		item_pool.append_array(common_items)
		var station_count = sorted_stations[biome].size()
		
		var passenger_station = network.get(
			sorted_stations[biome][rng.randi_range(0, station_count - 1)]
		)
		while passenger_station.is_main_station:
			passenger_station = network.get(
				sorted_stations[biome][rng.randi_range(0, station_count - 1)]
			)
		passenger_station.is_passenger_station = true
		
		for item in special_items:
			var has_space = false
			var item_station
			while not has_space:
				item_station = network.get(
					sorted_stations[biome][rng.randi_range(0, station_count - 1)]
				)
				if not item_station.is_passenger_station:
					if item_station.is_main_station and item_station.has_materials.size() < 3:
						has_space = true
					elif not item_station.is_main_station and item_station.has_materials.size() < 2:
						has_space = true
			item_station.has_materials.append(item)
		
		for station_key in sorted_stations[biome]:
			var station = network.get(station_key)
			if station.is_main_station:
				station.is_passenger_station = true
			if ((station.is_main_station and station.has_materials.size() == 3) or
				(station.is_passenger_station and not station.is_main_station) or
				(not station.is_main_station and station.has_materials.size() == 2)):
				continue
			
			
			if station.has_materials.size() > 0:
				if station.is_main_station:
					while station.has_materials.size() < 3:
						var item = item_pool[rng.randi() % item_pool.size()]
						if not item in station.has_materials:
							station.has_materials.append(item)
				else:
					while station.has_materials.size() < 2:
						var item = item_pool[rng.randi() % item_pool.size()]
						if not item in station.has_materials:
							station.has_materials.append(item)
			else:
				if rng.randf() < 0.5 and not station.is_main_station:
					station.is_passenger_station = true
				else:
					station.has_materials.append(special_items[rng.randi() % special_items.size()])
					if station.is_main_station:
						while station.has_materials.size() < 3:
							var item = item_pool[rng.randi() % item_pool.size()]
							if not item in station.has_materials:
								station.has_materials.append(item)
					else:
						while station.has_materials.size() < 2:
							var item = item_pool[rng.randi() % item_pool.size()]
							if not item in station.has_materials:
								station.has_materials.append(item)


func get_numbers():
	for i in range(1, 100):
		var current_station = {
			'pos': Vector2.ZERO,
			'connections': {
				'left': [],
				'right': []
			}
		}
		var ended = false
		while not ended:
			current_station.pos = Vector2(rng.randi_range(-500, 500), rng.randi_range(-500, 500))
			var cursta_pos = Vector2(current_station.pos.x, current_station.pos.y)
			var found_station := false
			for j in network.size():
				var compare_station = network.get(j)
				var comsta_pos = Vector2(compare_station.pos.x, compare_station.pos.y)
				if cursta_pos.distance_squared_to(comsta_pos) < MIN_DIST_SQUARED:
					found_station = true
					break
			if found_station:
				continue
			else:
				ended = true
		network[i] = current_station


func print_network():
	for i in network.size():
		print({'pos':network.get(i).pos,'connections':{'left':[],'right':[]},'biome':null})
#		prints(i, network.get(i).pos)
