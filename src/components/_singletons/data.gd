extends Node

# Static, shared data:

const GRID_SIZE = 3.2
const CURRENCY_SYMBOL = '¥'

var BIOMES = {
	ID.BIOME_PLAINS: {
		color = Color(0.15, 0.35, 0.15),
		decorations = [
			load('res://components/models/decorations/plains/decoration_plains_bush1.tscn'),
			load('res://components/models/decorations/plains/decoration_plains_bush2.tscn'),
			load('res://components/models/decorations/plains/decoration_plains_tree1.tscn'),
			load('res://components/models/decorations/plains/decoration_plains_tree2.tscn'),
			load('res://components/models/decorations/plains/decoration_plains_tree3.tscn'),
			load('res://components/models/decorations/plains/decoration_plains_cow.tscn'),
			load('res://components/models/decorations/plains/decoration_plains_field1.tscn'),
			load('res://components/models/decorations/plains/decoration_plains_field2.tscn'),
			load('res://components/models/decorations/plains/decoration_plains_field3.tscn'),
			load('res://components/models/decorations/plains/decoration_plains_gold.tscn'),
			load('res://components/models/giraffes/plains/giraffe_plains_base.tscn'),
		],
		passengers = [
			load('res://components/models/giraffes/plains/giraffe_plains_base.tscn'),
		],
		wheels = {
			name = 'Basic Wheels',
			costs = {}
		}
	},
	ID.BIOME_SEASIDE: {
		color = Color(0.7, 0.65, 0.5),
		color_water = Color(0.2, 0.2, 0.5),
		decorations = [
			load('res://components/models/decorations/seaside/decoration_seaside_starfish.tscn'),
			load('res://components/models/decorations/seaside/decoration_seaside_beach-umberella.tscn'),
			load('res://components/models/decorations/seaside/decoration_seaside_shells.tscn'),
			load('res://components/models/decorations/seaside/decoration_seaside_castle.tscn'),
			load('res://components/models/decorations/seaside/decoration_seaside_towel1.tscn'),
			load('res://components/models/decorations/seaside/decoration_seaside_towel2.tscn'),
			load('res://components/models/decorations/seaside/decoration_seaside_beach-towels.tscn'),
			load('res://components/models/decorations/seaside/decoration_seaside_boat.tscn'),
			load('res://components/models/decorations/desert/decoration_desert_sanddune.tscn'),
			load('res://components/models/giraffes/seaside/giraffe_seaside_base.tscn'),
			load('res://components/models/giraffes/seaside/giraffe_seaside_cactus.tscn'),
			load('res://components/models/giraffes/seaside/giraffe_seaside_rubber-ring.tscn'),
		],
		passengers = [
			load('res://components/models/giraffes/seaside/giraffe_seaside_base.tscn'),
			load('res://components/models/giraffes/seaside/giraffe_seaside_cactus.tscn'),
			load('res://components/models/giraffes/seaside/giraffe_seaside_rubber-ring.tscn'),
		],
		wheels = {
			name = 'Lifepreserverwheels',
			costs = {
				ID.MONEY : 10_000,
				ID.ITEM_GOLD : 5,
				ID.ITEM_BEEF : 5,
				ID.ITEM_WOOD : 10,
				ID.ITEM_BAMBOO : 10
			}
		}
	},
	ID.BIOME_MOUNTAINS: {
		color = Color(0.32, 0.35, 0.32),
		decorations = [
			load('res://components/models/decorations/mountain/decoration_mountain_grass.tscn'),
			load('res://components/models/decorations/mountain/decoration_mountain_rock1.tscn'),
			load('res://components/models/decorations/mountain/decoration_mountain_rock2.tscn'),
			load('res://components/models/decorations/plains/decoration_plains_gold.tscn'),
			load('res://components/models/giraffes/mountain/giraffe_mountain_base.tscn'),
			load('res://components/models/decorations/mountain/decoration_mountain_tree.tscn'),
		],
		passengers = [
			load('res://components/models/giraffes/mountain/giraffe_mountain_base.tscn'),
		],
		wheels = {
			name = 'Spikewheels',
			costs = {
				ID.MONEY : 70_000,
				ID.ITEM_BEER : 60,
				ID.ITEM_PIGEON_CONCENTRATE : 40,
				ID.ITEM_IRON : 35,
				ID.ITEM_NUCLEAR_ENERGY : 5
			}
		}
	},
	ID.BIOME_DESERT: {
		color = Color(0.8, 0.75, 0.60),
		decorations = [
			load('res://components/models/decorations/desert/decoration_desert_cactus1.tscn'),
			load('res://components/models/decorations/desert/decoration_desert_cactus2.tscn'),
			load('res://components/models/decorations/desert/decoration_desert_tumbleweed.tscn'),
			load('res://components/models/decorations/desert/decoration_desert_palm1.tscn'),
			load('res://components/models/decorations/desert/decoration_desert_palm2.tscn'),
			load('res://components/models/decorations/desert/decoration_desert_sanddune.tscn'),
			load('res://components/models/giraffes/desert/giraffe_desert_base.tscn'),
			load('res://components/models/giraffes/desert/giraffe_desert_cactus.tscn'),
			load('res://components/models/giraffes/desert/giraffe_desert_plain.tscn'),
			load('res://components/models/giraffes/desert/giraffe_desert_red-cacti.tscn'),
			load('res://components/models/giraffes/desert/giraffe_desert_small-cactus.tscn'),
		],
		passengers = [
			load('res://components/models/giraffes/desert/giraffe_desert_base.tscn'),
			load('res://components/models/giraffes/desert/giraffe_desert_cactus.tscn'),
			load('res://components/models/giraffes/desert/giraffe_desert_plain.tscn'),
			load('res://components/models/giraffes/desert/giraffe_desert_red-cacti.tscn'),
			load('res://components/models/giraffes/desert/giraffe_desert_small-cactus.tscn'),
		],
		wheels = {
			name = 'Shovelwheels',
			costs = {
				ID.MONEY : 25_000,
				ID.ITEM_SAND : 20,
				ID.ITEM_BEER : 20,
				ID.ITEM_STONE : 30,
				ID.ITEM_IRON : 10
			}
		}
	},
	ID.BIOME_VOLCANO: {
		color = Color(0.66, 0.23, 0.13),
		color_hot = Color(0.66, 0.26, 0.12),
		decorations = [
			load('res://components/models/decorations/volcano/decoration_volcano_rock.tscn'),
			load('res://components/models/decorations/volcano/decoration_volcano_smoke.tscn'),
			load('res://components/models/decorations/volcano/decoration_volcano_tree1.tscn'),
			load('res://components/models/decorations/volcano/decoration_volcano_tree2.tscn'),
			load('res://components/models/decorations/volcano/decoration_volcano_volcano.tscn'),
			load('res://components/models/decorations/plains/decoration_plains_gold.tscn'),
			load('res://components/models/decorations/volcano/decoration_volcano_opal.tscn'),
			load('res://components/models/decorations/volcano/decoration_volcano_diamond.tscn'),
			load('res://components/models/giraffes/volcano/giraffe_volcano_base.tscn'),
			load('res://components/models/giraffes/volcano/giraffe_volcano_ash.tscn'),
			load('res://components/models/giraffes/volcano/giraffe_volcano_dark_lava.tscn'),
			load('res://components/models/giraffes/volcano/giraffe_volcano_lava.tscn'),
		],
		passengers = [
			load('res://components/models/giraffes/volcano/giraffe_volcano_base.tscn'),
			load('res://components/models/giraffes/volcano/giraffe_volcano_ash.tscn'),
			load('res://components/models/giraffes/volcano/giraffe_volcano_dark_lava.tscn'),
			load('res://components/models/giraffes/volcano/giraffe_volcano_lava.tscn'),
		],
		wheels = {
			name = 'Firewheels',
			costs = {
				ID.MONEY : 500_000,
				ID.ITEM_OPAL : 100,
				ID.ITEM_PARROT : 50,
				ID.ITEM_NUCLEAR_ENERGY : 100,
				ID.ITEM_COPPER : 200,
				ID.ITEM_STONE : 200
			}
		}
	},
	ID.BIOME_UNDERWATER: {
		color = Color(0.36, 0.50, 0.66),
		decorations = [
			load('res://components/models/decorations/underwater/decoration_underwater_fish.tscn'),
			load('res://components/models/decorations/underwater/decoration_underwater_octopus.tscn'),
			load('res://components/models/decorations/underwater/decoration_underwater_seaweed.tscn'),
			load('res://components/models/decorations/underwater/decoration_underwater_fish2.tscn'),
			load('res://components/models/decorations/underwater/decoration_underwater_octopus2.tscn'),
			load('res://components/models/giraffes/underwater/giraffe_underwater_base.tscn'),
			load('res://components/models/giraffes/underwater/giraffe_underwater_scuba.tscn'),
		],
		passengers = [
			load('res://components/models/giraffes/underwater/giraffe_underwater_base.tscn'),
			load('res://components/models/giraffes/underwater/giraffe_underwater_scuba.tscn'),
		],
		wheels = {
			name = 'Propellerwheels',
			costs = {
				ID.MONEY : 100_000,
				ID.ITEM_GOLD : 50,
				ID.ITEM_KWARDIUM : 100,
				ID.ITEM_STONE : 100,
				ID.ITEM_COPPER : 50
			}
		}
	},
	ID.BIOME_RAINFOREST: {
		color = Color(0.1, 0.3, 0.05),
		decorations = [
			load('res://components/models/decorations/rainforest/decoration_rainforest_fern.tscn'),
			load('res://components/models/decorations/rainforest/decoration_rainforest_parrot.tscn'),
			load('res://components/models/decorations/rainforest/decoration_rainforest_tree1.tscn'),
			load('res://components/models/decorations/rainforest/decoration_rainforest_tree2.tscn'),
			load('res://components/models/decorations/rainforest/decoration_rainforest_tree3.tscn'),
			load('res://components/models/decorations/rainforest/decoration_rainforest_mound.tscn'),
			load('res://components/models/decorations/volcano/decoration_volcano_opal.tscn'),
			load('res://components/models/giraffes/rainforest/giraffe_rainforest_base.tscn'),
		],
		passengers = [
			load('res://components/models/giraffes/rainforest/giraffe_rainforest_base.tscn'),
		],
		wheels = {
			name = 'Vinewheels',
			costs = {
				ID.MONEY : 150_000,
				ID.ITEM_PIRATE_HAT : 75,
				ID.ITEM_HOOK : 65,
				ID.ITEM_BAMBOO : 200
			}
		}
	},
	ID.BIOME_CITY: {
		color = Color(0.3, 0.3, 0.3),
		decorations = [
			load('res://components/models/decorations/city/decoration_city_bike.tscn'),
			load('res://components/models/decorations/city/decoration_city_car.tscn'),
			load('res://components/models/decorations/city/decoration_city_pigeons.tscn'),
			load('res://components/models/decorations/city/decoration_city_skyscraper_big.tscn'),
			load('res://components/models/decorations/city/decoration_city_lamp1.tscn'),
			load('res://components/models/decorations/volcano/decoration_volcano_diamond.tscn'),
			load('res://components/models/giraffes/city/giraffe_city_base.tscn'),
			load('res://components/models/giraffes/city/giraffe_city_green.tscn'),
			load('res://components/models/giraffes/city/giraffe_city_pink.tscn'),
			load('res://components/models/giraffes/city/giraffe_city_red.tscn'),
		],
		passengers = [
			load('res://components/models/giraffes/city/giraffe_city_base.tscn'),
			load('res://components/models/giraffes/city/giraffe_city_green.tscn'),
			load('res://components/models/giraffes/city/giraffe_city_pink.tscn'),
			load('res://components/models/giraffes/city/giraffe_city_red.tscn'),
		],
		wheels = {
			name = 'Rubberwheels',
			costs = {
				ID.MONEY : 45_000,
				ID.ITEM_SAND : 30,
				ID.ITEM_SALT : 25,
				ID.ITEM_NUCLEAR_ENERGY : 20,
				ID.ITEM_COAL : 5
			}
		}
	},
	ID.BIOME_RAINBOW_ROAD: {
		color = Color(0.05, 0.05, 0.07),
		decorations = [
			load('res://components/models/decorations/rainbow/decoration_rainbow_rainbow-clouds.tscn'),
			load('res://components/models/decorations/rainbow/decoration_rainbow_rainbow-road.tscn'),
			load('res://components/models/decorations/rainbow/decoration_rainbow_unicorn.tscn'),
			load('res://components/models/giraffes/rainbow/giraffe_rainbow_base.tscn'),
			load('res://components/models/giraffes/rainbow/giraffe_rainbow_bold.tscn'),
			load('res://components/models/giraffes/rainbow/giraffe_rainbow_fade.tscn'),
		],
		passengers = [
			load('res://components/models/giraffes/rainbow/giraffe_rainbow_base.tscn'),
			load('res://components/models/giraffes/rainbow/giraffe_rainbow_bold.tscn'),
			load('res://components/models/giraffes/rainbow/giraffe_rainbow_fade.tscn'),
		],
		wheels = {
			name = 'Rainbowwheels',
			costs = {
				ID.MONEY : 2_500_000,
				ID.ITEM_GOLD : 500,
				ID.ITEM_OPAL : 250,
				ID.ITEM_WOOD : 100,
				ID.ITEM_COAL : 100,
				ID.ITEM_NUCLEAR_ENERGY : 100,
				ID.ITEM_BAMBOO : 100,
				ID.ITEM_COPPER : 100,
				ID.ITEM_IRON : 100,
				ID.ITEM_STONE : 100
			}
		}
	}
}

var TRACKS = {
	ID.TRACK_CURVE: [
		load('res://components/models/tracks/track_curve1.tscn'),
		load('res://components/models/tracks/track_curve2.tscn'),
	],
	ID.TRACK_JUNCTION: [
		load('res://components/models/tracks/track_junction1.tscn'),
	],
	ID.TRACK_STRAIGHT: [
		load('res://components/models/tracks/track_straight1.tscn'),
		load('res://components/models/tracks/track_straight2.tscn'),
	]
}

var TRAINS = {
	ID.TRAIN_1: {
		model = load('res://components/models/locomotives/steam/locomotive_steam_full.tscn'),
		costs = {
			ID.MONEY : 5_000,
			ID.ITEM_WOOD : 5,
			ID.ITEM_BEEF : 5,
			ID.ITEM_IRON : 2
		},
		strength = 350,
		tier = 1,
		name = 'Steam Locomotive'
	},
	ID.TRAIN_2: {
		model = load('res://components/models/locomotives/diesel/locomotive_diesel_full.tscn'),
		costs = {
			ID.MONEY : 25_000,
			ID.ITEM_PIGEON_CONCENTRATE : 5,
			ID.ITEM_SAND : 20,
			ID.ITEM_COPPER : 10,
			ID.ITEM_COAL : 5
		},
		strength = 1400,
		tier = 2,
		name = 'Electric Locomotive'
	},
	ID.TRAIN_3: {
		model = load('res://components/models/locomotives/electric/locomotive_electric_full.tscn'),
		costs = {
			ID.MONEY : 300_000,
			ID.ITEM_PARROT : 100,
			ID.ITEM_KWARDIUM : 50,
			ID.ITEM_HOOK : 50,
			ID.ITEM_IRON : 100
		},
		strength = 17500,
		tier = 3,
		name = 'Sophisticated Locomotive'
	}
}

var TRAIN_PROGRESSION = [ID.TRAIN_1, ID.TRAIN_2, ID.TRAIN_3]

var CARRIAGES = {
	ID.CARRIAGE_PLACEHOLDER: {
		model = load('res://components/models/carriages/carriage_placeholder.tscn'),
	},
	ID.CARRIAGE_CARGO_1: {
		model = load('res://components/models/carriages/cargo1/carriage_cargo1_full.tscn'),
		variants = {
			ID.CARGO1_VARIANT1: load('res://components/models/carriages/cargo1/carriage_cargo1_topcarriage.tscn'),
		},
		costs = {
			ID.MONEY : 1_000,
			ID.ITEM_STONE : 3,
			ID.ITEM_COAL : 1
		},
		capacity = 50,
		type = ID.CARRIAGE_TYPE_CARGO,
		tier = 1,
		name = 'Basic Carriage'
	},
	ID.CARRIAGE_CARGO_2: {
		model = load('res://components/models/carriages/cargo2/carriage_cargo2_full.tscn'),
		variants = {
			ID.CARGO2_VARIANT1: load('res://components/models/carriages/cargo2/carriage_cargo2_topcarriage-blue1.tscn'),
			ID.CARGO2_VARIANT2: load('res://components/models/carriages/cargo2/carriage_cargo2_topcarriage-blue2.tscn'),
			ID.CARGO2_VARIANT3: load('res://components/models/carriages/cargo2/carriage_cargo2_topcarriage-green1.tscn'),
			ID.CARGO2_VARIANT4: load('res://components/models/carriages/cargo2/carriage_cargo2_topcarriage-red1.tscn'),
			ID.CARGO2_VARIANT5: load('res://components/models/carriages/cargo2/carriage_cargo2_topcarriage-red2.tscn'),
		},
		costs = {
			ID.MONEY : 5_000,
			ID.ITEM_BEER : 5,
			ID.ITEM_BAMBOO : 5,
			ID.ITEM_IRON : 5
		},
		capacity = 200,
		type = ID.CARRIAGE_TYPE_CARGO,
		tier = 2,
		name = 'Advanced Carriage'
	},
	ID.CARRIAGE_CARGO_3: {
		model = load('res://components/models/carriages/cargo3/carriage_cargo3_full.tscn'),
		variants = {
			ID.CARGO3_VARIANT1: load('res://components/models/carriages/cargo3/carriage_cargo3_topcarriage.tscn'),
		},
		costs = {
			ID.MONEY : 60_000,
			ID.ITEM_OPAL : 35,
			ID.ITEM_SALT : 15,
			ID.ITEM_COPPER : 20
			},
		capacity = 2500,
		type = ID.CARRIAGE_TYPE_CARGO,
		tier = 3,
		name = 'Ultimate Carriage'
	},
	ID.CARRIAGE_PASSENGERS_1: {
		model = load('res://components/models/carriages/passenger1/carriage_passenger1_full.tscn'),
		variants = {
			ID.PASSENGER1_VARIANT1: load('res://components/models/carriages/passenger1/carriage_passenger1_topcarriage.tscn'),
		},
		costs = {
			ID.MONEY : 1_000,
			ID.ITEM_WOOD : 2,
			ID.ITEM_COAL : 1
		},
		capacity = 10,
		type = ID.CARRIAGE_TYPE_PASSENGER,
		tier = 1,
		name = 'Bearable Carriage'
	},
	ID.CARRIAGE_PASSENGERS_2: {
		model = load('res://components/models/carriages/passenger2/carriage_passenger2_full.tscn'),
		variants = {
			ID.PASSENGER2_VARIANT1: load('res://components/models/carriages/passenger2/carriage_passenger2_topcarriage1.tscn'),
			ID.PASSENGER2_VARIANT2: load('res://components/models/carriages/passenger2/carriage_passenger2_topcarriage2.tscn'),
		},
		costs = {
			ID.MONEY : 5_000,
			ID.ITEM_BEER : 5,
			ID.ITEM_BAMBOO : 3,
			ID.ITEM_IRON : 5,
			ID.ITEM_NUCLEAR_ENERGY : 1
		},
		capacity = 40,
		type = ID.CARRIAGE_TYPE_PASSENGER,
		tier = 2,
		name = 'Comfy Carriage'
	},
	ID.CARRIAGE_PASSENGERS_3: {
		model = load('res://components/models/carriages/passenger3/carriage_passenger3_full.tscn'),
		variants = {
			ID.PASSENGER3_VARIANT1: load('res://components/models/carriages/passenger3/carriage_passenger3_topcarriage.tscn'),
		},
		costs = {
			ID.MONEY : 60_000,
			ID.ITEM_OPAL : 35,
			ID.ITEM_PIRATE_HAT : 15,
			ID.ITEM_COPPER : 20
		},
		capacity = 500,
		type = ID.CARRIAGE_TYPE_PASSENGER,
		tier = 3,
		name = 'Luxurious Carriage'
	},
	ID.CARRIAGE_PARTY: {
		model = load('res://components/models/carriages/party/carriage_party_full.tscn'),
		variants = {
			ID.PARTY_VARIANT1: load('res://components/models/carriages/party/carriage_party_topcarriage.tscn'),
		},
		costs = {
			ID.MONEY : 10_000_000,
			ID.ITEM_FLOWER : 100,
			ID.ITEM_SEASHELL : 100,
			ID.ITEM_MOUNTAIN_GOAT : 100,
			ID.ITEM_LIVING_CACTUS : 100,
			ID.ITEM_DIAMOND : 100,
			ID.ITEM_TREASURE_CHEST : 100,
			ID.ITEM_LOVE : 100
		},
		type = ID.CARRIAGE_TYPE_PARTY,
		tier = 4,
		name = 'Party Carriage'
	}
}

var CARRIAGE_PROGRESSION = {
	ID.CARRIAGE_TYPE_CARGO: [ID.CARRIAGE_CARGO_1, ID.CARRIAGE_CARGO_2, ID.CARRIAGE_CARGO_3],
	ID.CARRIAGE_TYPE_PASSENGER: [ID.CARRIAGE_PASSENGERS_1, ID.CARRIAGE_PASSENGERS_2, ID.CARRIAGE_PASSENGERS_3],
	ID.CARRIAGE_TYPE_PARTY: [ID.CARRIAGE_PARTY]
}

var WHEELS_PROGRESSION = [
	ID.BIOME_PLAINS, ID.BIOME_SEASIDE, ID.BIOME_DESERT, ID.BIOME_CITY, ID.BIOME_MOUNTAINS, ID.BIOME_UNDERWATER,
	ID.BIOME_RAINFOREST, ID.BIOME_VOLCANO, ID.BIOME_RAINBOW_ROAD
]

var STATION_ELEMENTS = {
	ID.STATION_TYPE_CARGO: {
		ID.STATION_ELEMENT_WALL: [
			load('res://components/models/stations/cargo/station_cargo_back.tscn'),
		],
		ID.STATION_ELEMENT_FLOOR: [
			load('res://components/models/stations/cargo/station_cargo_middle.tscn'),
		],
		ID.STATION_ELEMENT_EDGE: [
			load('res://components/models/stations/cargo/station_cargo_edge.tscn')
		]
	},
	ID.STATION_TYPE_PASSANGER: {
		ID.STATION_ELEMENT_WALL: [
			load('res://components/models/stations/passenger/station_passenger_back.tscn'),
		],
		ID.STATION_ELEMENT_FLOOR: [
			load('res://components/models/stations/passenger/station_passenger_middle.tscn'),
		],
		ID.STATION_ELEMENT_EDGE: [
			load('res://components/models/stations/passenger/station_passenger_edge.tscn'),
		]
	},
	ID.STATION_TYPE_MAIN: {
		ID.STATION_ELEMENT_WALL: [
			load('res://components/models/stations/main/station_main_back1.tscn'),
			load('res://components/models/stations/main/station_main_back2.tscn'),
			load('res://components/models/stations/main/station_main_back3.tscn'),
		],
		ID.STATION_ELEMENT_FLOOR: [
			load('res://components/models/stations/main/station_main_middle.tscn'),
		],
		ID.STATION_ELEMENT_EDGE: [
			load('res://components/models/stations/main/station_main_edge.tscn'),
		]
	}
}

var ITEMS = {
	ID.COMMON_ITEM: [
		ID.ITEM_WOOD,
		ID.ITEM_COAL,
		ID.ITEM_NUCLEAR_ENERGY,
		ID.ITEM_BAMBOO,
		ID.ITEM_COPPER,
		ID.ITEM_IRON,
		ID.ITEM_STONE,
	],
	ID.SPECIAL_ITEM: [
		ID.ITEM_GOLD,
		ID.ITEM_BEEF,
		ID.ITEM_FLOWER,
		ID.ITEM_SAND,
		ID.ITEM_BEER,
		ID.ITEM_SEASHELL,
		ID.ITEM_KWARDIUM,
		ID.ITEM_MOUNTAIN_GOAT,
		ID.ITEM_LIVING_CACTUS,
		ID.ITEM_SALT,
		ID.ITEM_OPAL,
		ID.ITEM_DIAMOND,
		ID.ITEM_TREASURE_CHEST,
		ID.ITEM_PIRATE_HAT,
		ID.ITEM_HOOK,
		ID.ITEM_PARROT,
		ID.ITEM_PIGEON_CONCENTRATE,
		ID.ITEM_RAINBOW_DUST,
		ID.ITEM_LOVE,
		ID.ITEM_MAGIC_POTION,
	],
	ID.BIOME_PLAINS: [
		ID.ITEM_GOLD,
		ID.ITEM_BEEF,
		ID.ITEM_FLOWER,
	],
	ID.BIOME_SEASIDE: [
		ID.ITEM_SAND,
		ID.ITEM_BEER,
		ID.ITEM_SEASHELL,
	],
	ID.BIOME_MOUNTAINS: [
		ID.ITEM_GOLD,
		ID.ITEM_KWARDIUM,
		ID.ITEM_MOUNTAIN_GOAT,
	],
	ID.BIOME_DESERT: [
		ID.ITEM_SAND,
		ID.ITEM_LIVING_CACTUS,
		ID.ITEM_SALT,
	],
	ID.BIOME_VOLCANO: [
		ID.ITEM_GOLD,
		ID.ITEM_OPAL,
		ID.ITEM_DIAMOND,
	],
	ID.BIOME_UNDERWATER: [
		ID.ITEM_TREASURE_CHEST,
		ID.ITEM_PIRATE_HAT,
		ID.ITEM_HOOK,
	],
	ID.BIOME_RAINFOREST: [
		ID.ITEM_OPAL,
		ID.ITEM_TREASURE_CHEST,
		ID.ITEM_PARROT,
	],
	ID.BIOME_CITY: [
		ID.ITEM_DIAMOND,
		ID.ITEM_BEER,
		ID.ITEM_PIGEON_CONCENTRATE,
	],
	ID.BIOME_RAINBOW_ROAD: [
		ID.ITEM_RAINBOW_DUST,
		ID.ITEM_LOVE,
		ID.ITEM_MAGIC_POTION,
	]
}


var ITEMS_DATA = {
	ID.ITEM_WOOD: {
		name = 'Wood',
		weight = 1,
		icon = load('res://assets/icons/resources/wood.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 350, # money to buy one unit
		value = {
			same_biome = 400, # if its also sold in current biome
			different_biome = 400 # if its not sold in current biome
		}
	},
	ID.ITEM_COAL: {
		name = 'Coal',
		weight = 1,
		icon = load('res://assets/icons/resources/coal.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 450, # money to buy one unit
		value = {
			same_biome = 510, # if its also sold in current biome
			different_biome = 510 # if its not sold in current biome
		}
	},
	ID.ITEM_NUCLEAR_ENERGY: {
		name = 'Nuclear Energy',
		weight = 3,
		icon = load('res://assets/icons/resources/nuclear_energy.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 1950, # money to buy one unit
		value = {
			same_biome = 2250, # if its also sold in current biome
			different_biome = 2250 # if its not sold in current biome
		}
	},
	ID.ITEM_BAMBOO: {
		name = 'Bamboo',
		weight = 1,
		icon = load('res://assets/icons/resources/bamboo.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 300, # money to buy one unit
		value = {
			same_biome = 340, # if its also sold in current biome
			different_biome = 340 # if its not sold in current biome
		}
	},
	ID.ITEM_COPPER: {
		name = 'Copper',
		weight = 1,
		icon = load('res://assets/icons/resources/copper.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 550, # money to buy one unit
		value = {
			same_biome = 620, # if its also sold in current biome
			different_biome = 620 # if its not sold in current biome
		}
	},
	ID.ITEM_IRON: {
		name = 'Iron',
		weight = 1,
		icon = load('res://assets/icons/resources/iron.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 500, # money to buy one unit
		value = {
			same_biome = 570, # if its also sold in current biome
			different_biome = 570 # if its not sold in current biome
		}
	},
	ID.ITEM_STONE: {
		name = 'Stone',
		weight = 1,
		icon = load('res://assets/icons/resources/stone.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 200, # money to buy one unit
		value = {
			same_biome = 225, # if its also sold in current biome
			different_biome = 225 # if its not sold in current biome
		}
	},
	ID.ITEM_GOLD: {
		name = 'Gold',
		weight = 3,
		icon = load('res://assets/icons/resources/gold.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 3120, # money to buy one unit
		value = {
			same_biome = 3588, # if its also sold in current biome
			different_biome = 4350 # if its not sold in current biome
		}
	},
	ID.ITEM_BEEF: {
		name = 'Beef',
		weight = 2,
		icon = load('res://assets/icons/resources/beef.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 960, # money to buy one unit
		value = {
			same_biome = 1100, # if its also sold in current biome
			different_biome = 1340 # if its not sold in current biome
		}
	},
	ID.ITEM_FLOWER: {
		name = 'Flower',
		weight = 1,
		icon = load('res://assets/icons/resources/flower.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 120, # money to buy one unit
		value = {
			same_biome = 140, # if its also sold in current biome
			different_biome = 212 # if its not sold in current biome
		}
	},
	ID.ITEM_SAND: {
		name = 'Sand',
		weight = 1,
		icon = load('res://assets/icons/resources/sand.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 270, # money to buy one unit
		value = {
			same_biome = 310, # if its also sold in current biome
			different_biome = 375 # if its not sold in current biome
		}
	},
	ID.ITEM_BEER: {
		name = 'Beer',
		weight = 1,
		icon = load('res://assets/icons/resources/beer.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 444, # money to buy one unit
		value = {
			same_biome = 510, # if its also sold in current biome
			different_biome = 621 # if its not sold in current biome
		}
	},
	ID.ITEM_SEASHELL: {
		name = 'Seashell',
		weight = 2,
		icon = load('res://assets/icons/resources/seashells.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 1248, # money to buy one unit
		value = {
			same_biome = 1430, # if its also sold in current biome
			different_biome = 2180 # if its not sold in current biome
		}
	},
	ID.ITEM_KWARDIUM: {
		name = 'Kwardium',
		weight = 3,
		icon = load('res://assets/icons/resources/kwardium.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 1575, # money to buy one unit
		value = {
			same_biome = 1818, # if its also sold in current biome
			different_biome = 2205 # if its not sold in current biome
		}
	},
	ID.ITEM_MOUNTAIN_GOAT: {
		name = 'Mountain Goat',
		weight = 4,
		icon = load('res://assets/icons/resources/mountain_goat.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 6000, # money to buy one unit
		value = {
			same_biome = 6900, # if its also sold in current biome
			different_biome = 10500 # if its not sold in current biome
		}
	},
	ID.ITEM_LIVING_CACTUS: {
		name = 'Living Cactus',
		weight = 2,
		icon = load('res://assets/icons/resources/livingcactus.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 1998, # money to buy one unit
		value = {
			same_biome = 2300, # if its also sold in current biome
			different_biome = 3498 # if its not sold in current biome
		}
	},
	ID.ITEM_SALT: {
		name = 'Salt',
		weight = 1,
		icon = load('res://assets/icons/resources/salt.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 780, # money to buy one unit
		value = {
			same_biome = 900, # if its also sold in current biome
			different_biome = 1095 # if its not sold in current biome
		}
	},
	ID.ITEM_OPAL: {
		name = 'Opal',
		weight = 2,
		icon = load('res://assets/icons/resources/opal.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 1660, # money to buy one unit
		value = {
			same_biome = 1910, # if its also sold in current biome
			different_biome = 2322 # if its not sold in current biome
		}
	},
	ID.ITEM_DIAMOND: {
		name = 'Diamond',
		weight = 3,
		icon = load('res://assets/icons/resources/diamond.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 5265, # money to buy one unit
		value = {
			same_biome = 6066, # if its also sold in current biome
			different_biome = 9225 # if its not sold in current biome
		}
	},
	ID.ITEM_TREASURE_CHEST: {
		name = 'Treasure Chest',
		weight = 6,
		icon = load('res://assets/icons/resources/treasure_chest.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 13320, # money to buy one unit
		value = {
			same_biome = 15360, # if its also sold in current biome
			different_biome = 23394 # if its not sold in current biome
		}
	},
	ID.ITEM_PIRATE_HAT: {
		name = 'Pirate Hat',
		weight = 1,
		icon = load('res://assets/icons/resources/pirate_hat.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 1090, # money to buy one unit
		value = {
			same_biome = 1250, # if its also sold in current biome
			different_biome = 1526 # if its not sold in current biome
		}
	},
	ID.ITEM_HOOK: {
		name = 'Hook',
		weight = 2,
		icon = load('res://assets/icons/resources/hook.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 2598, # money to buy one unit
		value = {
			same_biome = 3000, # if its also sold in current biome
			different_biome = 3640 # if its not sold in current biome
		}
	},
	ID.ITEM_PARROT: {
		name = 'Parrot',
		weight = 3,
		icon = load('res://assets/icons/resources/parrot.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 4140, # money to buy one unit
		value = {
			same_biome = 4755, # if its also sold in current biome
			different_biome = 5799 # if its not sold in current biome
		}
	},
	ID.ITEM_PIGEON_CONCENTRATE: {
		name = 'Pigeon Concentrate',
		weight = 3,
		icon = load('res://assets/icons/resources/pigeonconcentrate.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 2589, # money to buy one unit
		value = {
			same_biome = 3003, # if its also sold in current biome
			different_biome = 3636 # if its not sold in current biome
		}
	},
	ID.ITEM_RAINBOW_DUST: {
		name = 'Rainbow Dust',
		weight = 2,
		icon = load('res://assets/icons/resources/rainbow_dust.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 2460, # money to buy one unit
		value = {
			same_biome = 2840, # if its also sold in current biome
			different_biome = 3444 # if its not sold in current biome
		}
	},
	ID.ITEM_LOVE: {
		name = 'Love',
		weight = 10,
		icon = load('res://assets/icons/resources/love.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 100000, # money to buy one unit
		value = {
			same_biome = 116660, # if its also sold in current biome
			different_biome = 177700 # if its not sold in current biome
		}
	},
	ID.ITEM_MAGIC_POTION: {
		name = 'Magic Potion',
		weight = 1,
		icon = load('res://assets/icons/resources/magic_potion.png'),
		available_in = [],
		is_money_item = false, # is true if the item exists for making much money
		costs = 6660, # money to buy one unit
		value = {
			same_biome = 7666, # if its also sold in current biome
			different_biome = 9325 # if its not sold in current biome
		}
	},
}


func _ready():
	fill_items_available_in()


func fill_items_available_in():
	for type in ITEMS:
		if type == ID.COMMON_ITEM:
			for item in ITEMS[type]:
				var item_data = ITEMS_DATA[item]
				for biome in BIOMES:
					item_data.available_in.append(biome)
		elif type != ID.SPECIAL_ITEM: # means type is a biome
			for item in ITEMS[type]:
				var item_data = ITEMS_DATA[item]
				item_data.available_in.append(type)
