extends Control


onready var resume_button = $'%ResumeButton'
onready var options_button = $'%OptionsButton'
onready var save_button = $'%SaveButton'
onready var exit_button = $'%ExitButton'

onready var v_separator = $'%VSeparator'
onready var options_view = $'%OptionsView'
onready var save_view = $'%SaveView'

onready var options_blur = $'%Blur'
onready var options_volume = $'%VolumeSlider'
onready var options_volume_value = $'%VolumeValue'

onready var save_name = $'%SaveName'
onready var save_menu_button = $'%SaveMenuButton'


func _ready():
	resume_button.connect('pressed', self, '__toggle_visible')
	options_button.connect('pressed', self, '__toggle_options_view')
	save_button.connect('pressed', self, '__toggle_save_view')
	exit_button.connect('pressed', self, '__back_to_menu')
	
	options_volume.connect('value_changed', self, '__set_volume')
	options_blur.connect('toggled', self, '__set_blur')
	
	save_menu_button.connect('pressed', self, '__save_and_quit')


func __toggle_visible():
	if is_visible_in_tree():
		hide()
	else:
		show()
		__refresh_options()


func __refresh_options():
	options_blur.set_pressed(SAVESTATE.get_blur())
	options_volume.set_value(SAVESTATE.get_volume())
	options_volume_value.set_text(str(SAVESTATE.get_volume()))


func __toggle_options_view():
	if options_view.is_visible_in_tree():
		options_view.hide()
		v_separator.hide()
	else:
		options_view.show()
		v_separator.show()


func __toggle_save_view():
	if save_view.is_visible_in_tree():
		save_view.hide()
		v_separator.hide()
	else:
		save_view.show()
		v_separator.show()


func __set_volume(new_value: float):
	SAVESTATE.set_volume(new_value)
	options_volume.set_value(new_value)
	options_volume_value.set_text(str(new_value))


func __set_blur(is_active: bool):
	SAVESTATE.set_blur(is_active)
	options_blur.set_pressed(is_active)


func __save_and_quit():
	SAVESTATE.generate_save(save_name.get_text())
	__back_to_menu()


func __back_to_menu():
	SCENE.goto_scene('menu')


func _unhandled_input(event):
	if event.is_action_pressed('ui_cancel'):
		__toggle_visible()
