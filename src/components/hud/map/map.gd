extends Control


const RESIZE_SPEED := 0.5
const TRAIN_MARKER_SPEED := 5
const TRAIN_MARKER_EXPRESS_SPEED := 10

onready var stations_node: Node2D = $Viewport/Stations
onready var connections_node: Node2D = $Viewport/Connections
onready var station_names_node: Node2D = $Viewport/StationNames
onready var train_marker_node: Node2D = $Viewport/TrainMarker
onready var train_node: Sprite = $Viewport/TrainMarker/Train
onready var view_node: TextureRect = $View

var map_big = false

var STATION_ICONS = {
	'passengers': load('res://assets/icons/map/passenger_icon2.png'),
	'main': load('res://assets/icons/map/mainstation_icon2.png'),
	'cargo': load('res://assets/icons/map/cargo_icon2.png'),
	'hidden': load('res://assets/icons/map/hiddenstation_icon.png')
}


var finished_moves = [
	'anchorleft',
	'anchortop',
	'anchorright',
	'anchorbottom',
	'marginleft',
	'margintop',
	'marginright',
	'marginbottom',
	'view_anchorleft',
	'view_anchortop',
	'view_anchorright',
	'view_anchorbottom',
	'view_marginleft',
	'view_margintop',
	'view_marginright',
	'view_marginbottom',
	'nodes',
	'names_opacity'
]
var current_map_pos = Vector2(125, 125)
var map_states = {
	'big': {
		'anchor': {
			'left': 0,
			'top': 0,
			'right': 1,
			'bottom': 1
		},
		'margin': {
			'left': 0,
			'top': 0,
			'right': 0,
			'bottom': 0
		},
		'view_anchor': {
			'left': 0.5,
			'top': 0,
			'right': 0.5,
			'bottom': 1
		},
		'view_margin': {
			'left': -540,
			'top': 0,
			'right': 540,
			'bottom': 0
		},
		'nodes': Vector2(540, 540),
		'names_opacity': 1
	},
	'small': {
		'anchor': {
			'left': 1,
			'top': 1,
			'right': 1,
			'bottom': 1
		},
		'margin': {
			'left': -265,
			'top': -265,
			'right': -15,
			'bottom': -15
		},
		'view_anchor': {
			'left': 0,
			'top': 0,
			'right': 1,
			'bottom': 1
		},
		'view_margin': {
			'left': 0,
			'top': 0,
			'right': 0,
			'bottom': 0
		},
		'nodes': Vector2(125, 125), # is current map pos, changes at runtime
		'names_opacity': 0
	}
}

var train_marker_pos = {
	'old': Vector2.ZERO,
	'new': Vector2.ZERO
}

func _ready():
	$Background.connect('gui_input', self, '__handle_gui_input')
	EVENTS.connect('changed_current_station', self, '_set_current_map_pos')
	EVENTS.connect('passed_junction', self, '_set_new_dest', [0])
	EVENTS.connect('depart_station', self, '_set_new_dest', [1])
	_set_current_map_pos(0)
	draw_map()


func draw_map():
	for child in stations_node.get_children():
		child.queue_free()
	for child in station_names_node.get_children():
		child.queue_free()
	for child in connections_node.get_children():
		child.queue_free()
	
	var already_drawn = []
	for i in range(RAIL_NETWORK.network.size()):
		var data = RAIL_NETWORK.network.get(i)
		if not data.visible:
			continue
		
		if data.is_main_station:
			add_point(data.pos, 'main', data.name)
		elif data.is_passenger_station:
			add_point(data.pos, 'passengers', data.name)
		else:
			add_point(data.pos, 'cargo', data.name)
			
		if not data.center_points.left in already_drawn:
			add_connection(data.pos, data.center_points.left)
			for k in data.connections.left:
				var goal_data = RAIL_NETWORK.network.get(k)
				add_connection(goal_data.pos, data.center_points.left)
				if not goal_data.visible:
					add_point(goal_data.pos, 'hidden')
			already_drawn.append(data.center_points.left)
		
		if not data.center_points.right in already_drawn:
			add_connection(data.pos, data.center_points.right)
			for k in data.connections.right:
				var goal_data = RAIL_NETWORK.network.get(k)
				add_connection(goal_data.pos, data.center_points.right)
				if not goal_data.visible:
					add_point(goal_data.pos, 'hidden')
			already_drawn.append(data.center_points.right)


func _unhandled_input(event):
	if event.is_action_pressed('show_map'):
		__toggle_map()


func __handle_gui_input(event: InputEvent):
	if event is InputEventMouseButton and event.get_button_index() == BUTTON_LEFT and event.is_pressed():
		__toggle_map()


func __toggle_map():
	map_big = !map_big
	finished_moves = []


func _set_current_map_pos(id):
	var station = RAIL_NETWORK.network.get(id)
	station.visible = true
	map_states.small.nodes = Vector2(125, 125) - station.pos
	
	if STATE.get_has_arrived_by_left_connection():
		rotate_train(station.pos, station.center_points.right)
	else:
		rotate_train(station.pos, station.center_points.left)
	
	draw_map()


func rotate_train(curr_pos, dest):
	var dir_vec = curr_pos - dest
	var radians = Vector2.LEFT.angle_to(dir_vec)
	train_node.set_rotation(radians)
	if radians < -PI / 2 or radians > PI / 2:
		train_node.set_flip_v(true)
	else:
		train_node.set_flip_v(false)


func _set_new_dest(type: int):
	if type == 0: # to destination station
		var new_station = RAIL_NETWORK.network.get(STATE._target_station_id)
		var old_station = STATE.get_current_station()
		train_marker_pos.new = new_station.pos
		if STATE.get_has_arrived_by_left_connection():
			train_marker_pos.old = old_station.center_points.right
			rotate_train(old_station.center_points.right, new_station.pos)
		else:
			train_marker_pos.old = old_station.center_points.left
			rotate_train(old_station.center_points.left, new_station.pos)
	elif type == 1: # to center point
		var current_station = STATE.get_current_station()
		if (current_station.is_main_station and
			STATE.get_target_station_id() in current_station.connections.express):
			train_marker_pos.new = STATE.get_target_station().pos
			rotate_train(current_station.pos, train_marker_pos.new)
		var center_point = STATE.get_current_station().center_points.left
		if STATE.get_has_arrived_by_left_connection():
			center_point = STATE.get_current_station().center_points.right
		train_marker_pos.new = center_point


func add_point(pos: Vector2, type: String, station_name: String = ''):
	var new_sprite = Sprite.new()
	new_sprite.set_position(pos) 
	new_sprite.set_texture(STATION_ICONS[type])
	new_sprite.set_scale(Vector2(0.2, 0.2))
	stations_node.add_child(new_sprite)
	if station_name != '':
		var new_label = Label.new()
		new_label.set_text(station_name)
		new_label.set_size(Vector2(120, 20))
		new_label.set_align(Label.ALIGN_CENTER)
		new_label.set_valign(Label.VALIGN_CENTER)
		new_label.set_position(pos + Vector2(-60, -30))
		station_names_node.add_child(new_label)


func add_connection(start: Vector2, end: Vector2):
	var new_connection = Line2D.new()
	new_connection.add_point(start)
	new_connection.add_point(end)
	new_connection.set_width(3)
	new_connection.set_default_color(Color.slategray)
	connections_node.add_child(new_connection)


func set_map_pos(new_pos: Vector2):
	stations_node.set_position(new_pos)
	connections_node.set_position(new_pos)
	station_names_node.set_position(new_pos)
	train_marker_node.set_position(new_pos)
	current_map_pos = new_pos


func _process(delta):
	var data
	var prev_data
	if map_big:
		data = map_states['big']
		prev_data = map_states['small']
	else:
		data = map_states['small']
		prev_data = map_states['big']
	
	_set_anchors(self, data.anchor, prev_data.anchor, delta, 'anchor')
	_set_margins(self, data.margin, prev_data.margin, delta, 'margin')
	_set_anchors(view_node, data.view_anchor, prev_data.view_anchor, delta, 'view_anchor')
	_set_margins(view_node, data.view_margin, prev_data.view_margin, delta, 'view_margin')
	
	if not 'nodes' in finished_moves:
		var dist = data.nodes - prev_data.nodes
		if dist >= Vector2.ZERO:
			if current_map_pos >= data.nodes:
				set_map_pos(data.nodes)
				finished_moves.append('nodes')
			else:
				set_map_pos(current_map_pos + (dist / RESIZE_SPEED) * delta)
		else:
			if current_map_pos <= data.nodes:
				set_map_pos(data.nodes)
				finished_moves.append('nodes')
			else:
				set_map_pos(current_map_pos + (dist / RESIZE_SPEED) * delta)
	
	if not 'names_opacity' in finished_moves:
		var dist = data.names_opacity - prev_data.names_opacity
		var old_value = station_names_node.get_modulate().a
		if dist >= 0:
			if old_value >= data.names_opacity:
				station_names_node.set_modulate(Color(1, 1, 1, data.names_opacity))
				finished_moves.append('names_opacity')
			else:
				station_names_node.set_modulate(Color(1, 1, 1, old_value + (dist / RESIZE_SPEED) * delta))
		else:
			if old_value <= data.names_opacity:
				station_names_node.set_modulate(Color(1, 1, 1, data.names_opacity))
				finished_moves.append('names_opacity')
			else:
				station_names_node.set_modulate(Color(1, 1, 1, old_value + (dist / RESIZE_SPEED) * delta))
	
	if train_marker_pos.old != train_marker_pos.new:
		var speed = TRAIN_MARKER_SPEED
		if STATE.train_is_on_express():
			speed = TRAIN_MARKER_EXPRESS_SPEED
		var dist = train_marker_pos.new - train_marker_pos.old
		var old_value = train_node.get_position()
		if dist >= Vector2.ZERO:
			if old_value >= train_marker_pos.new:
				train_node.set_position(train_marker_pos.new)
				train_marker_pos.old = train_marker_pos.new
			else:
				train_node.set_position(old_value + (dist / speed) * delta)
		else:
			if old_value <= train_marker_pos.new:
				train_node.set_position(train_marker_pos.new)
				train_marker_pos.old = train_marker_pos.new
			else:
				train_node.set_position(old_value + (dist / speed) * delta)
	
		if not map_big:
			set_map_pos(Vector2(125, 125) - train_node.get_position())


func _set_anchors(obj, dest: Dictionary, start: Dictionary, delta: float, id: String):
	_set_single_anchor(obj, MARGIN_LEFT, dest.left, dest.left - start.left, delta, id + 'left')
	_set_single_anchor(obj, MARGIN_TOP, dest.top, dest.top - start.top, delta, id + 'top')
	_set_single_anchor(obj, MARGIN_RIGHT, dest.right, dest.right - start.right, delta, id + 'right')
	_set_single_anchor(obj, MARGIN_BOTTOM, dest.bottom, dest.bottom - start.bottom, delta, id + 'bottom')


func _set_margins(obj, dest: Dictionary, start: Dictionary, delta: float, id: String):
	_set_single_margin(obj, MARGIN_LEFT, dest.left, dest.left - start.left, delta, id + 'left')
	_set_single_margin(obj, MARGIN_TOP, dest.top, dest.top - start.top, delta, id + 'top')
	_set_single_margin(obj, MARGIN_RIGHT, dest.right, dest.right - start.right, delta, id + 'right')
	_set_single_margin(obj, MARGIN_BOTTOM, dest.bottom, dest.bottom - start.bottom, delta, id + 'bottom')


func _set_single_anchor(obj, margin, dest: float, dist: float, delta: float, id: String):
	if id in finished_moves:
		return
	
	var old_value = obj.get_anchor(margin)
	if dist >= 0:
		if old_value >= dest:
			obj.set_anchor(margin, dest, true)
			finished_moves.append(id)
		else:
			obj.set_anchor(margin, old_value + (dist / RESIZE_SPEED) * delta, true)
	else:
		if old_value <= dest:
			obj.set_anchor(margin, dest, true)
			finished_moves.append(id)
		else:
			obj.set_anchor(margin, old_value + (dist / RESIZE_SPEED) * delta, true)


func _set_single_margin(obj, margin, dest: float, dist: float, delta: float, id: String):
	if id in finished_moves:
		return
	
	var old_value = obj.get_margin(margin)
	if dist >= 0:
		if old_value >= dest:
			obj.set_margin(margin, dest)
			finished_moves.append(id)
		else:
			obj.set_margin(margin, old_value + (dist / RESIZE_SPEED) * delta)
	else:
		if old_value <= dest:
			obj.set_margin(margin, dest)
			finished_moves.append(id)
		else:
			obj.set_margin(margin, old_value + (dist / RESIZE_SPEED) * delta)
