extends Control


onready var title = $'%Title'

onready var cargo_tab = $'%CargoTab'
onready var passengers_tab = $'%PassengersTab'
onready var sell_tab = $'%SellTab'
onready var upgrades_tab = $'%UpgradesTab'
onready var cargo_view = $'%CargoView'
onready var passengers_view = $'%PassengersView'
onready var sell_view = $'%SellView'
onready var upgrades_view = $'%UpgradesView'
onready var tabs_views = {
	ID.TAB_CARGO: [cargo_tab, cargo_view],
	ID.TAB_PASSENGERS: [passengers_tab, passengers_view],
	ID.TAB_SELL: [sell_tab, sell_view],
	ID.TAB_UPGRADE: [upgrades_tab, upgrades_view]
}
onready var tabs_jeffrey_degrees = {
	ID.TAB_CARGO: 50,
	ID.TAB_PASSENGERS: 55,
	ID.TAB_SELL: 60,
	ID.TAB_UPGRADE: 65
}
onready var jeffrey = $'%Jeffrey'


func _ready():
	cargo_tab.connect('pressed', self, '__set_visible', [ID.TAB_CARGO])
	passengers_tab.connect('pressed', self, '__set_visible', [ID.TAB_PASSENGERS])
	sell_tab.connect('pressed', self, '__set_visible', [ID.TAB_SELL])
	upgrades_tab.connect('pressed', self, '__set_visible', [ID.TAB_UPGRADE])

	EVENTS.connect('arrive_station', self, '__on_arrive_station')
	EVENTS.connect('depart_station', self, '__on_depart_station')

	__set_visible(ID.TAB_CARGO)
	__on_arrive_station()


func _unhandled_input(_event):
	if Input.is_action_just_pressed('ui_accept') and visible:
		__on_pressed_go()


func __on_pressed_go():
	EVENTS.emit_signal('depart_station')


func __set_visible(id):
	for tv in tabs_views.values():
		tv[0].deselect()
		tv[1].hide()
	
	var selected_tv = tabs_views[id]
	selected_tv[0].select()
	selected_tv[1].show()

	jeffrey.rotation_degrees.y = tabs_jeffrey_degrees[id]


func __on_depart_station():
	hide()


func __on_arrive_station():
	show()

	var station_data = STATE.get_current_station()

	if station_data.is_main_station:
		tabs_views[ID.TAB_UPGRADE][0].enable()
	else:
		tabs_views[ID.TAB_UPGRADE][0].disable()

	if station_data.is_passenger_station or station_data.is_main_station:
		tabs_views[ID.TAB_PASSENGERS][0].enable()
		__set_visible(ID.TAB_PASSENGERS)
	else:
		tabs_views[ID.TAB_PASSENGERS][0].disable()

	if station_data.has_materials.size() > 0 or station_data.is_main_station:
		tabs_views[ID.TAB_CARGO][0].enable()
		__set_visible(ID.TAB_CARGO)
	else:
		tabs_views[ID.TAB_CARGO][0].disable()
