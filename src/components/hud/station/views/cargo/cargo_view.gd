extends Control


onready var no_cargo_label = $'%NoCargoLabel'
onready var cargo_container = $'%CargoContainer'
onready var cargo_card_1 = $'%CargoCard1'
onready var cargo_card_2 = $'%CargoCard2'
onready var cargo_card_3 = $'%CargoCard3'
onready var cargo_cards = [
	cargo_card_1, cargo_card_2, cargo_card_3
]
onready var total_cargo_cost_label = $'%TotalCargoCostLabel'
onready var total_cargo_weight_label = $'%TotalCargoWeightLabel'


var ressource_card_scene = preload('res://components/hud/station/partials/ressource_card.tscn')
var ressource_scene = preload('res://components/hud/station/partials/ressource.tscn')


func _ready():
	EVENTS.connect('arrive_station', self, '__on_arrive_station')

	$'%CargoButton'.connect('pressed', self, '__on_cargo_commit')

	__on_arrive_station()

	cargo_card_1.connect('count_changed', self, '__on_cargo_card_count_changed', [cargo_card_1])
	cargo_card_2.connect('count_changed', self, '__on_cargo_card_count_changed', [cargo_card_2])
	cargo_card_3.connect('count_changed', self, '__on_cargo_card_count_changed', [cargo_card_3])


func __on_arrive_station():
	var station_data = STATE.get_current_station()

	if station_data.has_materials.size():
		cargo_container.show()
		no_cargo_label.hide()
		for i in station_data.has_materials.size():
			var item_id = station_data.has_materials[i]
			var item_data = DATA.ITEMS_DATA[item_id]
			cargo_cards[i].set_data(item_id, 0, item_data.costs)
		if station_data.has_materials.size() == 2:
			cargo_card_3.hide()
			cargo_card_3.set_count(0)
		else: 
			cargo_card_3.show()
	else:
		no_cargo_label.show()
		cargo_container.hide()


func __on_cargo_commit():
	var station_data = STATE.get_current_station()

	for i in station_data.has_materials.size():
		var count = cargo_cards[i].get_count()
		if count == 0:
			continue
		var id = station_data.has_materials[i]
		STATE.change_inventory({
			id = id,
			count = count
		})
		STATE.change_money(-1 * DATA.ITEMS_DATA[id].costs * count)
		cargo_cards[i].set_count(0)


func __on_cargo_card_count_changed(old_count, count, node):
	var station_data = STATE.get_current_station()

	if count == 0:
		return

	var total_cost = 0
	for i in station_data.has_materials.size():
		var item_count = cargo_cards[i].get_count()
		if item_count == 0:
			continue
		var id = station_data.has_materials[i]
		total_cost += DATA.ITEMS_DATA[id].costs * item_count
	
	if total_cost > STATE.get_money():
		if abs(old_count - count) == 1.0:
			node.set_count(old_count, true)
		else:
			node.set_count(0, true)
