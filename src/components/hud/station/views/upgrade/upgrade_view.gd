extends Control


onready var no_upgrades_label = $'%NoUpgradesLabel'
onready var scroll_container = $'%ScrollContainer'
onready var entries_container = $'%UpgradesContainer'


var upgrade_scene = preload('res://components/hud/station/views/upgrade/upgrade_card.tscn')
var upgrade_entry_scene = preload('res://components/hud/station/views/upgrade/upgrade_entry.tscn')


func _ready():
	EVENTS.connect('arrive_station', self, '__on_arrive_station')
	EVENTS.connect('set_carriages', self, '__on_set_train_composition')
	EVENTS.connect('set_train', self, '__on_set_train_composition')

	__on_set_train_composition()


func __on_arrive_station():
	var station_data = STATE.get_current_station()

	if station_data.is_main_station:
		no_upgrades_label.hide()
		scroll_container.show()
	else:
		no_upgrades_label.show()
		scroll_container.hide()


func __on_set_train_composition():
	var data = STATE.get_carriages()

	while entries_container.get_child_count() > 0:
		var c = entries_container.get_child(0)
		entries_container.remove_child(c)
		c.queue_free()

	for i in data.size():
		var nc = upgrade_scene.instance()
		nc.set_data(data[i].id, false)
		entries_container.add_child(nc)
		nc.connect('upgrade_pressed', self, '__on_upgrade_pressed_for_carriage', [i])

	var nlc = upgrade_scene.instance()
	nlc.set_data(STATE.get_train(), true)
	entries_container.add_child(nlc)
	nlc.connect('upgrade_pressed', self, '__on_upgrade_pressed_for_locomotice')

	var ne = upgrade_entry_scene.instance()
	ne.set_data(ID.CARRIAGE_PARTY, true, false)
	entries_container.add_child(ne)
	ne.connect('upgrade_pressed', self, '__on_party')


func __on_upgrade_pressed_for_carriage(new_id, i):
	STATE.change_carriage(i, {id = new_id})


func __on_upgrade_pressed_for_locomotice(new_id):
	STATE.set_train(new_id)

func __on_party():
	$'%Party'.show()
