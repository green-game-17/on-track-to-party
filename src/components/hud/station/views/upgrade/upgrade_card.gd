extends Control


signal upgrade_pressed(new_id)

var entry_scene = preload('res://components/hud/station/views/upgrade/upgrade_entry.tscn')


func set_data(id, is_locomotive: bool):
	var data
	if is_locomotive:
		data = DATA.TRAINS[id]
	else:
		data = DATA.CARRIAGES[id]

	var carriage_anchor = $'%CarriageAnchor'
	while carriage_anchor.get_child_count() > 0:
		var c = carriage_anchor.get_child(0)
		carriage_anchor.remove_child(c)
		c.queue_free()
	var nc = data.model.instance()
	carriage_anchor.add_child(nc)

	if not is_locomotive:
		if data.type == ID.CARRIAGE_TYPE_CARGO:
			$'%CargoInfo'.show()
			$'%PassengerInfo'.hide()
		elif data.type == ID.CARRIAGE_TYPE_PASSENGER:
			$'%PassengerInfo'.show()
			$'%CargoInfo'.hide()

	$'%TitleLabel'.set_text(data.name)
	$'%TierLabel'.set_text('Mk %d'%data.tier)
	if not is_locomotive:
		$'%CapacityLabel'.set_text('%dt'%data.capacity)
	else:
		$'%CapacityLabel'.set_text('∞')


	var progression
	if is_locomotive:
		progression = DATA.TRAIN_PROGRESSION
	else:
		progression = DATA.CARRIAGE_PROGRESSION[data.type]

	var progression_index = 0
	for i in progression.size():
		if progression[i] == id:
			progression_index = i
	
	var entries_container = $'%EntriesContainer'
	while entries_container.get_child_count() > 0:
		var c = entries_container.get_child(0)
		entries_container.remove_child(c)
		c.queue_free()
	for i in progression.size():
		if i > progression_index:
			var ne = entry_scene.instance()
			var enable_upgrade = true if i == progression_index + 1 else false
			ne.set_data(progression[i], enable_upgrade, is_locomotive)
			ne.connect('upgrade_pressed', self, '__on_entry_upgraded', [progression[i]])
			entries_container.add_child(ne)


func __on_entry_upgraded(id):
	emit_signal('upgrade_pressed', id)
