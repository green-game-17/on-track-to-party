extends Control


onready var money_label = $'%MoneyLabel'
onready var inventory_items_container = $'%inventoryItemsContainer'
onready var weight_label = $'%WeightLabel'
onready var weight_progress_bar = $'%WeightProgressBar'


var ressource_scene = preload('res://components/hud/station/partials/ressource.tscn')


func _ready():
	EVENTS.connect('money_changed', self, '__on_money_changed')
	EVENTS.connect('inventory_changed', self, '__on_inventory_changed')
	$'%GoButton'.connect('pressed', self, '__on_pressed_go')

	__on_money_changed()
	__on_inventory_changed()


func __on_money_changed():
	money_label.set_text('%s%s'%[
		UTIL.format_count(STATE.get_money()),
		DATA.CURRENCY_SYMBOL
	])


func __on_inventory_changed():
	while inventory_items_container.get_child_count() > 0:
		var c = inventory_items_container.get_child(0)
		inventory_items_container.remove_child(c)
		c.queue_free()

	var total_weight = 0
	for item in STATE.get_inventory():
		var n = ressource_scene.instance()
		n.set_data(item.id, item.count, 0)
		inventory_items_container.add_child(n)
		var data = DATA.ITEMS_DATA[item.id]
		total_weight += item.count * data.weight

	var total_capacity = 0
	for carriage in STATE.get_carriages():
		var data = DATA.CARRIAGES[carriage.id]
		if data.type == ID.CARRIAGE_TYPE_CARGO:
			total_capacity += data.capacity
			
	weight_label.set_text('%st'%[
		UTIL.format_count(total_weight)
	])
	weight_progress_bar.max_value = total_capacity
	weight_progress_bar.set_value(total_weight)


func __on_pressed_go():
	EVENTS.emit_signal('depart_station')
