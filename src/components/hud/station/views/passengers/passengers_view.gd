extends Control


onready var no_passengers_label = $'%NoPassengersLabel'
onready var passengers_container = $'%PassengersContainer'
onready var passenger_card_1 = $'%PassengerCard1'
onready var passenger_card_2 = $'%PassengerCard2'
onready var passenger_card_3 = $'%PassengerCard3'
onready var passenger_cards = [
	passenger_card_1, passenger_card_2, passenger_card_3
]
onready var total_passenger_count_label = $'%TotalPassengerCountLabel'


func _ready():
	EVENTS.connect('arrive_station', self, '__on_arrive_station')
	EVENTS.connect('passengers_changed', self, '__on_passengers_changed')

	$'%PassengersButton'.connect('pressed', self, '__on_passengers_commit')

	__on_arrive_station()


func __on_arrive_station():
	var station_data = STATE.get_current_station()

	if station_data.is_passenger_station:
		STATE.change_money(STATE.get_passenger_amount() * 75)
		STATE.set_passenger_amount(0)
		passengers_container.show()
		no_passengers_label.hide()
		passenger_cards[0].set_data('somewhere')
		total_passenger_count_label.set_text('0')
	else:
		no_passengers_label.show()
		passengers_container.hide()


func __on_passengers_changed():
	var count = 0
	for c in passenger_cards:
		count += c.get_count()
	if count > STATE.get_free_passenger_capacity():
		var diff = count - STATE.get_free_passenger_capacity()
		for c in passenger_cards:
			if c.get_count() >= diff:
				c.set_count(c.get_count() - diff)
				break
			elif c.get_count() > 0:
				diff -= c.get_count()
				c.set_count(0)
		count = STATE.get_free_passenger_capacity()


func __on_passengers_commit():
	var passengers = 0
	for c in passenger_cards:
		passengers += c.get_count()
	STATE.change_passenger_amount(passengers)
	total_passenger_count_label.set_text('%d'%[STATE.get_passenger_amount()])
	for c in passenger_cards:
		c.set_count(0)
