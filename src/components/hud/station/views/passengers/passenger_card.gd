extends Control


onready var value_edit = $'%ValueEdit'


func _ready():
	$'%PlusButton'.connect('pressed', self, '__on_button', [+1])
	$'%MinusButton'.connect('pressed', self, '__on_button', [-1])


func set_data(target: String, count: int = 0):
	$'%Label'.set_text('travelling to %s'%[target])
	$'%ValueEdit'.set_text('%d'%count)


func set_count(count: int):
	value_edit.set_text('%d'%count)


func get_count() -> int:
	return value_edit.get_text().to_int()


func __on_button(value: int):
	value_edit.set_text('%d'%[ max(value_edit.get_text().to_int() + value, 0) ])
	EVENTS.emit_signal('passengers_changed')
