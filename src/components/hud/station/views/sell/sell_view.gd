extends Control


onready var common_cargo_container = $'%CommonCargoContainer'
onready var specific_cargo_container = $'%SpecificCargoContainer'
var sell_item_entries = {}


var ressource_card_scene = preload('res://components/hud/station/partials/ressource_card.tscn')


func _ready():
	EVENTS.connect('arrive_station', self, '__on_arrive_station')

	$'%SellButton'.connect('pressed', self, '__on_sell_commit')

	while common_cargo_container.get_child_count() > 0:
		var c = common_cargo_container.get_child(0)
		common_cargo_container.remove_child(c)
		c.queue_free()
	while specific_cargo_container.get_child_count() > 0:
		var c = specific_cargo_container.get_child(0)
		specific_cargo_container.remove_child(c)
		c.queue_free()

	for key in DATA.ITEMS[ID.COMMON_ITEM]:
		var n = ressource_card_scene.instance()
		n.set_data(key, 0, 0)
		sell_item_entries[key] = n
		common_cargo_container.add_child(n)
		n.connect('count_changed', self, '__on_sell_card_count_changed', [n])
	for key in DATA.ITEMS[ID.SPECIAL_ITEM]:
		var n = ressource_card_scene.instance()
		n.set_data(key, 0, 0)
		sell_item_entries[key] = n
		specific_cargo_container.add_child(n)
		n.connect('count_changed', self, '__on_sell_card_count_changed', [n])

	__on_arrive_station()


func __on_arrive_station():
	var station_data = STATE.get_current_station()

	for key in sell_item_entries.keys():
		var data = DATA.ITEMS_DATA[key]
		var value = 0
		if key in station_data.has_materials:
			value = floor(data.costs * 0.5)
		elif station_data.biome in data.available_in:
			value = data.value.same_biome
		else:
			value = data.value.different_biome
		sell_item_entries[key].set_cost(value)
		sell_item_entries[key].set_count(0)


func __on_sell_commit():
	for key in sell_item_entries.keys():
		var count = sell_item_entries[key].get_count()
		if count == 0:
			continue
		STATE.change_inventory({
			id = key,
			count = -1 * count
		})
		STATE.change_money(sell_item_entries[key].get_cost() * count)
		sell_item_entries[key].set_count(0)


func __on_sell_card_count_changed(old_count, count, node):
	if count == 0:
		return

	var id = node.get_id()

	var valid = false
	for item in STATE.get_inventory():
		if item.id == id:
			if count <= item.count:
				valid = true
				break

	if not valid:
		if abs(old_count - count) == 1.0:
			node.set_count(old_count, true)
		else:
			node.set_count(0, true)
