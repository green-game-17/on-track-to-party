tool
extends ToolButton


export var title: String = 'Hi'
export var background_color: Color = Color.forestgreen
export var icon_texture: Texture = null

onready var paper = $'%Paper'
onready var icon_texture_rect = $'%IconTexture'


func _ready():
	$'%TitleLabel'.set_text(title)
	paper.modulate = background_color
	if icon_texture:
		icon_texture_rect.set_texture(icon_texture)
		var new_material = icon_texture_rect.material.duplicate()
		icon_texture_rect.set_material(new_material)


func disable():
	icon_texture_rect.material.set_shader_param('do_desaturate', true)
	paper.modulate = Color(0.4, 0.4, 0.4)


func enable():
	icon_texture_rect.material.set_shader_param('do_desaturate', false)
	paper.modulate = background_color


func select():
	paper.modulate.a = 1.0


func deselect():
	paper.modulate.a = 0.8
