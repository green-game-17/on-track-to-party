extends Control


func set_data(id, count: int = 0):
	var data = DATA.ITEMS_DATA[id]

	$'%IconTexture'.set_texture(data.icon)
	$'%NameLabel'.set_text(data.name)
	
	$'%CountLabel'.set_text('%d'%count)
	if count:
		$'%CountLabel'.show()
	else:
		$'%CountLabel'.hide()
