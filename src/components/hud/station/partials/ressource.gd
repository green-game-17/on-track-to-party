extends Control


func set_data(id, count: int = 0, cost: int = 0):
	var data = DATA.ITEMS_DATA[id]

	$'%IconTexture'.set_texture(data.icon)
	$'%NameLabel'.set_text(data.name)

	if count:
		$'%CountLabel'.show()
		$'%CountLabel'.set_text('%d'%count)
	else:
		$'%CountLabel'.hide()
	
	set_cost(cost)

	var display_count = count if count else 1
	$'%WeightLabel'.set_text('%dt'%[data.weight * display_count])


func set_cost(cost):
	if cost:
		$'%CostContainer'.show()
		$'%CostLabel'.set_text('%d%s'%[cost, DATA.CURRENCY_SYMBOL])
	else:
		$'%CostContainer'.hide()
