extends Spatial


export (bool) var topcarriage_visible = true


onready var topcarriage = $Variants

var wheels = []
var wheel_speed = 0
var moving_speed = 0


func _ready():
	var wheel_amount = 1
	while get_node_or_null('Wheel' + str(wheel_amount)) != null:
		var wheel_node = get_node('Wheel' + str(wheel_amount))
		wheels.append({
			'node': wheel_node,
			'radius': abs(wheel_node.get_child(0).get_translation().y)
		})
		wheel_amount += 1
	
	refresh_topcarriage()


func set_wheel_speed(new_speed: float):
	wheel_speed = new_speed


func set_moving_speed(new_speed: float):
	moving_speed = new_speed


func set_speed(new_speed: float):
	wheel_speed = new_speed
	moving_speed = new_speed


func toggle_topcarriage(is_visible: bool):
	topcarriage_visible = is_visible
	refresh_topcarriage()


func refresh_topcarriage():
	if topcarriage_visible:
		topcarriage.show()
	else:
		topcarriage.hide()


func set_variant(carriage_type: String, variant_id: String):
	for child in topcarriage.get_children():
		child.queue_free()
	topcarriage.add_child(DATA.CARRIAGES[carriage_type].variants[variant_id].instance())


func hide_jeffrey():
	var jeffrey = get_node_or_null('Jeffrey')
	if jeffrey != null:
		jeffrey.hide()


func _process(delta):
	if wheels.size() > 0 and wheel_speed > 0:
		for wheel in wheels:
			var radius = wheel.radius - 0.1
			var half_rotations = wheel_speed / (PI * radius)
			wheel.node.rotate_z(-delta * PI * half_rotations)
	if moving_speed > 0:
		var pos = get_translation()
		pos.z += -delta * moving_speed
		set_translation(pos)
