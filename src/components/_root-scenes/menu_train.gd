extends Spatial


var speed = 0


func _ready():
	pass


func set_speed(new_speed: float):
	speed = new_speed
	for child in get_children():
		child.set_wheel_speed(new_speed)


func set_appearance(train_id, new_menu_type):
	for child_id in get_child_count():
		match new_menu_type:
			ID.MENU_TYPE_MAIN:
				if child_id == 2:
					get_child(child_id).toggle_topcarriage(false)
				else:
					get_child(child_id).toggle_topcarriage(true)
			ID.MENU_TYPE_LOAD:
				if train_id in [0, 2] and child_id == 2:
					get_child(child_id).toggle_topcarriage(false)
				elif train_id == 1 and child_id in [1, 2, 3]:
					get_child(child_id).toggle_topcarriage(false)
				else:
					get_child(child_id).toggle_topcarriage(true)
			ID.MENU_TYPE_OPTIONS:
				if train_id in [0, 2] and child_id == 2:
					get_child(child_id).toggle_topcarriage(false)
				elif train_id == 1 and child_id in [1, 2, 3]:
					get_child(child_id).toggle_topcarriage(false)
				else:
					get_child(child_id).toggle_topcarriage(true)


func _process(delta):
	if speed > 0:
		var pos = get_translation()
		pos.z += -delta * speed
		set_translation(pos)
