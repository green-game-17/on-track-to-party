extends HBoxContainer


var speed = 0


func _ready():
	pass


func set_speed(new_speed: float):
	speed = new_speed


func _process(delta):
	if speed > 0:
		var pos = get_position()
		pos.x += delta * speed
		set_position(pos)
