extends Control


const TRAIN_RESET_Z = -50
const TRAIN_STOP_Z = -12.75
const TRAIN_SLOW_Z = -5.75
const TRAIN_FAST_Z = -20
const TRAIN_SPEED = 30

const BOX_STOP_X = 460
const BOX_SPEED = 1600

onready var start_button = $'%MainStartButton'
onready var options_button = $'%MainOptionsButton'
onready var exit_button = $'%MainExitButton'
onready var back_button = $'%BackButton'
onready var blur_checkbox = $'%OptionsCheckBox'
onready var volume_slider = $'%OptionsHSlider'
onready var volume_label = $'%OptionsVolume'
onready var new_game_button = $'%LoadNewButton'
onready var load_buttons = [
	$'%LoadButton1',
	$'%LoadButton2',
	$'%LoadButton3',
]
onready var trains = [
	$'%Train1',
	$'%Train2',
	$'%Train3'
]
onready var boxes = [
	$'%Box0',
	$'%Box1',
	$'%Box2'
]

var previous_menu_type = ID.MENU_TYPE_MAIN
var current_menu_type = ID.MENU_TYPE_MAIN
var button_clicked = false
var train_data = [
	{
		'reset_z': 30,
		'resetted': false,
		'time_delay': 0,
		'time_delay_counter': 0,
		'button_pos': -1830,
	},
	{
		'reset_z': 35,
		'resetted': false,
		'time_delay': 0.333,
		'time_delay_counter': 0,
		'button_pos': -2095,
	},
	{
		'reset_z': 40,
		'resetted': false,
		'time_delay': 0.666,
		'time_delay_counter': 0,
		'button_pos': -2365,
	},
]


func _ready():
	start_button.connect('pressed', self, '__show_load')
	options_button.connect('pressed', self, '__show_options')
	back_button.connect('pressed', self, '__back_to_menu')
	blur_checkbox.connect('toggled', self, '__set_blur')
	volume_slider.connect('value_changed', self, '__volume_changed')
	new_game_button.connect('pressed', self, '__on_start')
	
	for button_id in load_buttons.size():
		var button = load_buttons[button_id]
		button.connect('pressed', self, '__on_start', [button_id])
	
	$'%Train2/CarriageCargo2Full3'.set_variant(ID.CARRIAGE_CARGO_2, ID.CARGO2_VARIANT4)
	$'%Train2/CarriageCargo2Full4'.set_variant(ID.CARRIAGE_CARGO_2, ID.CARGO2_VARIANT3)
	
	for train_id in trains.size():
		var train = trains[train_id]
		train.set_appearance(train_id, current_menu_type)
		train.set_speed(TRAIN_SPEED)
		if train_id > 0:
			train.get_child(0).hide_jeffrey()
	for box_id in boxes.size():
		var box = boxes[box_id]
		box.set_speed(BOX_SPEED)


func __on_start(load_id: int = -1):
	if SAVESTATE.get_saves().size() <= load_id:
		return
	
	if load_id >= 0:
		prints('TODO: load save', load_id)
	SCENE.goto_scene('story')


func __show_options():
	button_clicked = true
	previous_menu_type = current_menu_type
	current_menu_type = ID.MENU_TYPE_OPTIONS


func __show_load():
	button_clicked = true
	previous_menu_type = current_menu_type
	current_menu_type = ID.MENU_TYPE_LOAD


func __back_to_menu():
	button_clicked = true
	previous_menu_type = current_menu_type
	current_menu_type = ID.MENU_TYPE_MAIN


func __set_blur(new_value: bool, save: bool = true):
	SAVESTATE.set_blur(new_value, save)
	blur_checkbox.set_pressed(new_value)


func __volume_changed(new_value, save: bool = true):
	SAVESTATE.set_volume(new_value, save)
	volume_slider.set_value(new_value)
	volume_label.set_text(str(new_value))


func __fill_saves():
	var saves = SAVESTATE.get_saves()
	print(saves)
	for button_id in load_buttons.size():
		var button = load_buttons[button_id]
		if saves.size() > button_id:
			button.set_text(saves[button_id].name)
		else:
			button.set_text('Empty')


func start_trains():
	var resetted_count = 0
	for train_id in trains.size():
		if train_data[train_id].resetted:
			resetted_count += 1
	if resetted_count == trains.size():
		button_clicked = false
		match previous_menu_type:
			ID.MENU_TYPE_MAIN:
				get_tree().call_group('MenuMain', 'hide')
			ID.MENU_TYPE_OPTIONS:
				get_tree().call_group('MenuOptions', 'hide')
			ID.MENU_TYPE_LOAD:
				get_tree().call_group('MenuLoad', 'hide')
		match current_menu_type:
			ID.MENU_TYPE_MAIN:
				get_tree().call_group('MenuMain', 'show')
			ID.MENU_TYPE_OPTIONS:
				get_tree().call_group('MenuOptions', 'show')
				__volume_changed(SAVESTATE.get_volume(), false)
				__set_blur(STATE.blur_active, false)
			ID.MENU_TYPE_LOAD:
				get_tree().call_group('MenuLoad', 'show')
				__fill_saves()
		for train_id in trains.size():
			var train = trains[train_id]
			var box = boxes[train_id]
			train_data[train_id].resetted = false
			train.set_appearance(train_id, current_menu_type)
			train.set_speed(TRAIN_SPEED)
			box.set_speed(BOX_SPEED)


func _process(delta):
	for train_id in trains.size():
		var train = trains[train_id]
		var box = boxes[train_id]
		var pos = train.get_translation()
		var box_pos = box.get_position()
		if train_data[train_id].resetted or (pos.z == TRAIN_STOP_Z and not button_clicked):
			continue
		
		if button_clicked:
			if train.speed == TRAIN_SPEED:
				if pos.z < TRAIN_RESET_Z:
					pos.z = train_data[train_id].reset_z
					box_pos.x = train_data[train_id].button_pos
					train.set_translation(pos)
					box.set_position(box_pos)
					train.set_speed(0)
					box.set_speed(0)
					train_data[train_id].time_delay_counter = 0
					train_data[train_id].resetted = true
					start_trains()
			elif pos.z < TRAIN_FAST_Z:
				train.set_speed(TRAIN_SPEED)
				box.set_speed(BOX_SPEED)
			else:
				if train_data[train_id].time_delay_counter < train_data[train_id].time_delay:
					train_data[train_id].time_delay_counter += delta
					if train_data[train_id].time_delay_counter > train_data[train_id].time_delay:
						train_data[train_id].time_delay_counter = train_data[train_id].time_delay
				else:
					var speed_up_dist = TRAIN_FAST_Z - TRAIN_STOP_Z
					var remaining_dist = TRAIN_FAST_Z - pos.z
					var speed_part_train = TRAIN_SPEED / 30
					var speed_part_box = BOX_SPEED / 23
					train.set_speed(max((TRAIN_SPEED - speed_part_train) * (1.0 - (remaining_dist / speed_up_dist)) + speed_part_train, train.speed))
					box.set_speed(max((BOX_SPEED - speed_part_box) * (1.0 - (remaining_dist / speed_up_dist)) + speed_part_box, box.speed))
		elif pos.z < TRAIN_STOP_Z:
			train.set_speed(0)
			pos.z = TRAIN_STOP_Z
			train.set_translation(pos)
			box.set_speed(0)
			box_pos.x = BOX_STOP_X
			box.set_position(box_pos)
		elif pos.z < TRAIN_SLOW_Z:
			var slow_down_dist = TRAIN_STOP_Z - TRAIN_SLOW_Z
			var remaining_dist = TRAIN_STOP_Z - pos.z
			var speed_part_train = TRAIN_SPEED / 30
			var speed_part_box = BOX_SPEED / 23
			train.set_speed((TRAIN_SPEED - speed_part_train) * (remaining_dist / slow_down_dist) + speed_part_train)
			box.set_speed((BOX_SPEED - speed_part_box) * (remaining_dist / slow_down_dist) + speed_part_box)
