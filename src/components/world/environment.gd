extends Spatial


onready var rng = RandomNumberGenerator.new()
onready var is_debug_enabled = OS.has_feature('editor')

onready var ground_anchor = $GroundAnchor
onready var ground_tile_base = $GroundAnchor/GroundBlock
onready var train_anchor = $TrainAnchor


const HEIGHT = 25.0
const VIEWPORT_WIDTH = 52.0
const WIDTH = 170
const TRANSITION_WIDTH = 20.0
const DECORATION_PROBABILITY = 0.05
const TRACK_Z = 15
const JUNCTION_X = 115
const TRAIN_Y = 0.15
const TRAIN_MAX_OFFSET_CARRIAGES = 9

const TRANSITION_START = JUNCTION_X - TRANSITION_WIDTH/2.0
const TRANSITION_END = JUNCTION_X + TRANSITION_WIDTH/2.0
const STATION_Z_START = TRACK_Z - 3
const STATION_Z_END = TRACK_Z + 2
const STATION_WIDTH = (TRAIN_MAX_OFFSET_CARRIAGES + 1) * 2 + 4
const STATION_A_X_START = int(VIEWPORT_WIDTH/2) - int(STATION_WIDTH/2)
const STATION_A_X_END = STATION_A_X_START + STATION_WIDTH - 1
const STATION_B_X_START = JUNCTION_X + 5
const STATION_B_X_END = STATION_B_X_START + STATION_WIDTH - 1


var tiles = []
var last_updated_row_x = 0


func _ready():
	rng.randomize()

	__generate_base()
	__update_train()

	EVENTS.connect('set_train', self, '__update_train')
	EVENTS.connect('set_carriages', self, '__update_train')

	EVENTS.connect('depart_station', self, '__loop_b_to_a')


func update_for_camera(x):
	var left_edge_in_grid = stepify(x - (VIEWPORT_WIDTH*DATA.GRID_SIZE/2), DATA.GRID_SIZE) / DATA.GRID_SIZE
	if left_edge_in_grid >= WIDTH - VIEWPORT_WIDTH:
		return

	for ix in left_edge_in_grid:
		if tiles[ix] == null:
			continue
		elif tiles[ix][0].get_translation().x > x:
			break
		else:
			__update_row(ix)

	__update_train_position(x)


func __generate_base():
	tiles.resize(WIDTH)

	var station = STATE.get_current_station()

	for ix in range(0, VIEWPORT_WIDTH):
		tiles[ix] = []
		tiles[ix].resize(HEIGHT)
		for iz in range(0, HEIGHT):
			var new_ground_tile = ground_tile_base.duplicate()
			new_ground_tile.show()
			ground_anchor.add_child(new_ground_tile)
			tiles[ix][iz] = new_ground_tile
			__set_tile(ix, iz, new_ground_tile, station, station, 1, 0)
			new_ground_tile.set_translation(Vector3(
				ix * DATA.GRID_SIZE,
				0,
				iz * DATA.GRID_SIZE
			))


func __update_row(original_ix):
	var ix = original_ix + VIEWPORT_WIDTH - 1
	tiles[ix] = []
	tiles[ix].resize(HEIGHT)

	var current_station = STATE.get_current_station()
	var target_station = STATE.get_target_station()
	var paths_count = STATE.get_paths_count()
	var selected_path = STATE.get_selected_path()

	for iz in range(0, HEIGHT):
		var ground_tile = tiles[original_ix][iz]
		tiles[ix][iz] = ground_tile
		__set_tile(ix, iz, ground_tile, current_station, target_station, paths_count, selected_path)
		ground_tile.set_translation(Vector3(
			ix * DATA.GRID_SIZE,
			0,
			iz * DATA.GRID_SIZE
		))

	tiles[original_ix] = null
	last_updated_row_x = original_ix


func __loop_b_to_a():
	if last_updated_row_x < 1:
		return # initial state

	for ix in VIEWPORT_WIDTH - 1:
		last_updated_row_x += 1
		var old_tiles = tiles[last_updated_row_x]
		if not old_tiles:
			break
		tiles[ix] = old_tiles.duplicate()
		for t in tiles[ix]:
			t.set_translation(Vector3(ix * DATA.GRID_SIZE, 0, t.get_translation().z))
		tiles[last_updated_row_x] = null


func __set_tile(
	ix: int, iz: int, 
	ground_tile: Spatial, 
	current_station, target_station, 
	paths_count: int, selected_path: int
):
	var new_material = ground_tile.get_active_material(0).duplicate()
	ground_tile.set_surface_material(0, new_material)

	while ground_tile.get_child_count() > 0:
		var old_decoration = ground_tile.get_child(0)
		ground_tile.remove_child(old_decoration)
		old_decoration.queue_free()

	var ix_f = float(ix)

	var is_in_station = false
	if (
		(
			ix >= STATION_A_X_START and ix <= STATION_A_X_END
		) or (
			ix >= STATION_B_X_START and ix <= STATION_B_X_END
		)
	) and (iz >= STATION_Z_START and iz <= STATION_Z_END):
		is_in_station = true

	var station
	if ix_f < TRANSITION_START:
		station = current_station
	elif ix_f > TRANSITION_END:
		station = target_station
	else:
		if rng.randf() > ease(((ix_f-TRANSITION_START)/TRANSITION_WIDTH), -2.0):
			station = current_station
		else:
			station = target_station
	
	var biome = DATA.BIOMES[station.biome]
	var color = biome.color
	if biome.has('color_water') and (
		rng.randf_range(0.7, 1.0) > sin(ix_f/7.0+5) and iz > HEIGHT - sin(ix_f/7.0+5) * (HEIGHT / 3)
	):
		color = biome.color_water
	elif biome.has('color_hot') and (
		rng.randf() > 0.7
	):
		color = biome.color_hot
	new_material.albedo_color = color.darkened(rng.randf_range(0.0, 0.1))

	if is_in_station:
		var station_type = ID.STATION_TYPE_CARGO
		if station.is_passenger_station:
			station_type = ID.STATION_TYPE_PASSANGER
		if station.is_main_station:
			station_type = ID.STATION_TYPE_MAIN

		var node
		if iz == STATION_Z_START:
			if station_type != ID.STATION_TYPE_MAIN:
				node = __pick_and_add_as_child(ground_tile, DATA.STATION_ELEMENTS[station_type][ID.STATION_ELEMENT_WALL])
			else:
				var scene = DATA.STATION_ELEMENTS[ID.STATION_TYPE_MAIN][ID.STATION_ELEMENT_WALL][1]
				var sx = ix - STATION_Z_START
				var m = (sx - 4) % 5
				if sx > 2 and sx < 22 and  m == 1 or m == 3:
					scene = DATA.STATION_ELEMENTS[ID.STATION_TYPE_MAIN][ID.STATION_ELEMENT_WALL][2]
				elif sx > 2 and sx < 22 and  m == 2:
					scene = DATA.STATION_ELEMENTS[ID.STATION_TYPE_MAIN][ID.STATION_ELEMENT_WALL][0]
				node = __add_as_child(ground_tile, scene)
		elif iz == STATION_Z_START + 1:
			node = __pick_and_add_as_child(ground_tile, DATA.STATION_ELEMENTS[station_type][ID.STATION_ELEMENT_FLOOR])
		elif iz == STATION_Z_START + 2:
			node = __pick_and_add_as_child(ground_tile, DATA.STATION_ELEMENTS[station_type][ID.STATION_ELEMENT_EDGE])
		elif iz == STATION_Z_START + 3:
			node = __pick_and_add_track_as_child(ground_tile, 'h')
		elif iz == STATION_Z_END - 1:
			node = __pick_and_add_as_child(ground_tile, DATA.STATION_ELEMENTS[station_type][ID.STATION_ELEMENT_EDGE])
			node.rotate_y(PI)
		elif iz == STATION_Z_END:
			node = __pick_and_add_as_child(ground_tile, DATA.STATION_ELEMENTS[station_type][ID.STATION_ELEMENT_FLOOR])
			node.rotate_y(PI)

		if node:
			var mesh = node.get_child(0)
			var new_node_material = mesh.get_active_material(0).duplicate()
			mesh.set_surface_material(0, new_node_material)
			new_node_material.albedo_color = Color.white.darkened(0.25).darkened(rng.randf_range(0.0, 0.05))

	elif ix == JUNCTION_X and paths_count > 1:
		if iz == TRACK_Z:
			if selected_path == 0:
				__pick_and_add_track_as_child(ground_tile, 'jd')
			else:
				__pick_and_add_track_as_child(ground_tile, 'ju')
		else:
			if selected_path == 0:
				if iz > TRACK_Z:
					__pick_and_add_track_as_child(ground_tile, 'v')
			elif iz < TRACK_Z: 
				__pick_and_add_track_as_child(ground_tile, 'v')
	
	elif ix == JUNCTION_X + 2 and paths_count == 3:
		if iz == TRACK_Z:
			if selected_path < 2:
				__pick_and_add_track_as_child(ground_tile, 'jd')
			else:
				__pick_and_add_track_as_child(ground_tile, 'ju')
		else:
			if selected_path < 2:
				if iz > TRACK_Z:
					__pick_and_add_track_as_child(ground_tile, 'v')
			elif iz < TRACK_Z: 
				__pick_and_add_track_as_child(ground_tile, 'v')

	elif iz == TRACK_Z:
		__pick_and_add_track_as_child(ground_tile, 'h')

	elif rng.randf() < DECORATION_PROBABILITY:
		var decoration_node = __pick_and_add_as_child(ground_tile, biome.decorations)
		if decoration_node:
			decoration_node.rotate_y(PI / 2 * rng.randi_range(0, 3))


func __pick_and_add_track_as_child(ground_tile, track_type):
	var tracks = null
	var rotation = 0
	var translation = Vector3.ZERO
	match track_type:
		'h': # horizontal
			tracks = DATA.TRACKS[ID.TRACK_STRAIGHT]
			rotation = PI / 2
		'ju': # junction - up
			tracks = DATA.TRACKS[ID.TRACK_JUNCTION]
			rotation = PI
			translation.z = -0.2
		'jd': # junction - down
			tracks = DATA.TRACKS[ID.TRACK_JUNCTION]
			rotation = PI * 2
			translation.x = -0.2
		'cu': # curve - from up
			tracks = DATA.TRACKS[ID.TRACK_CURVE]
			rotation = PI * 2
		'cd': # curve - from down
			tracks = DATA.TRACKS[ID.TRACK_CURVE]
			rotation = PI * 0.5
			translation.z = -0.2
		'v': # vertical
			tracks = DATA.TRACKS[ID.TRACK_STRAIGHT]
			rotation = 0
		_:
			pass

	if tracks != null:
		var track_node = __pick_and_add_as_child(ground_tile, tracks)
		if track_node:
			track_node.rotate_y(rotation)
			track_node.set_translation(translation)


func __pick_and_add_as_child(parent, arr):
	if arr.size() == 0:
		return null
	
	var ri = int(arr.size() * rng.randf())
	var child = arr[ri].instance()
	parent.add_child(child)
	return child


func __add_as_child(parent, scene):
	var child = scene.instance()
	parent.add_child(child)
	return child


func __update_train_position(x):
	var x_offset = clamp(STATE.get_carriages().size() * DATA.GRID_SIZE, 0.0, TRAIN_MAX_OFFSET_CARRIAGES * DATA.GRID_SIZE)
	x_offset += DATA.GRID_SIZE / 2
	train_anchor.set_translation(Vector3(x + x_offset, TRAIN_Y, TRACK_Z * DATA.GRID_SIZE))


func __update_train():
	var train_state = STATE.get_train()
	var carriages_state = STATE.get_carriages()

	var train_data = DATA.TRAINS[train_state]
	var carriages_data = []
	for c in carriages_state:
		carriages_data.append(DATA.CARRIAGES[c.id])

	while train_anchor.get_child_count() > 0:
		var old_node = train_anchor.get_child(0)
		train_anchor.remove_child(old_node)
		old_node.queue_free()

	train_anchor.add_child(train_data.model.instance())
	for i in carriages_data.size():
		var c_node = carriages_data[i].model.instance()
		train_anchor.add_child(c_node)
		c_node.set_translation(Vector3((i + 1) * -2.0 * DATA.GRID_SIZE, 0, 0))
