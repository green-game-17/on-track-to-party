extends Spatial


onready var start_x = get_translation().x


const MAX_FRAME_COUNT = 60.0 * 10.0
const MAX_SPEED = 64.0
const BASE_SPEED = 24.0
const MAX_TRAIL_LENGTH = 50
const ACCELERATE_RANGE = 0.25


var frame_count = 0


func reset():
	frame_count = 0
	var t = get_translation()
	set_translation(Vector3(
		start_x,
		t.y,
		t.z
	))


func update_speed_and_position(delta):
	if frame_count > MAX_FRAME_COUNT:
		return {
			speed = 0.0,
			x = get_translation().x,
			trail_length = 0,
			arrived = true
		}
		
	var step := 0.0 
	var speed := 0.0
	var trail_length := 0

	# see https://raw.githubusercontent.com/godotengine/godot-docs/3.5/img/ease_cheatsheet.png

	if frame_count < MAX_FRAME_COUNT * ACCELERATE_RANGE:
		step = ease(__get_frame_fraction(0.0), 2)
		speed = step * BASE_SPEED
	elif frame_count < MAX_FRAME_COUNT * ACCELERATE_RANGE * 2:
		step = ease(__get_frame_fraction(1.0), 0.2)
		speed = BASE_SPEED + step * MAX_SPEED
		trail_length = int(step * MAX_TRAIL_LENGTH)
	elif frame_count < MAX_FRAME_COUNT * ACCELERATE_RANGE * 3:
		step = (1.0 - ease(__get_frame_fraction(2.0), 0.2))
		speed = BASE_SPEED + step * MAX_SPEED
		trail_length = int(step * MAX_TRAIL_LENGTH)
	else:
		step = (1.0 - ease(__get_frame_fraction(3.0), 2))
		speed = step * BASE_SPEED

	var t = get_translation()
	var x = t.x + speed*delta
	set_translation(Vector3(x, t.y, t.z))

	frame_count += 1

	return {
		speed = speed,
		x = x,
		trail_length = trail_length,
		arrived = false
	}


func __get_frame_fraction(start_multiplier):
	return (frame_count - start_multiplier * ACCELERATE_RANGE * MAX_FRAME_COUNT) / (MAX_FRAME_COUNT * ACCELERATE_RANGE)
